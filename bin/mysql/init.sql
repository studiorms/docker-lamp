-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: cyfe-api-push-db-1.ctjgy3dx7shp.us-east-1.rds.amazonaws.com    Database: cyfe
-- ------------------------------------------------------
-- Server version	5.5.53-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE IF NOT EXISTS cyfe_api_push_db_test;
USE cyfe_api_push_db_test;

--
-- Table structure for table `api_push_queue`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `api_push_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `payload` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `widget_id` (`widget_id`),
  KEY `date_added` (`date_added`)
) ENGINE=InnoDB AUTO_INCREMENT=115026024572115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'cyfe'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 21:05:55
-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: cyfe-customer-db-1.ctjgy3dx7shp.us-east-1.rds.amazonaws.com    Database: cyfe
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE IF NOT EXISTS cyfe_customer_db_test;
USE cyfe_customer_db_test;

--
-- Table structure for table `gradeus_reviews`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `gradeus_reviews` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `site_id` bigint(20) NOT NULL,
  `review_directory` enum('facebook','google','yelp','yp','trip advisor','bbb','amazon','homeadvisor','healthgrades') NOT NULL DEFAULT 'google',
  `review_profile_url` varchar(300) NOT NULL,
  `review_id` varchar(100) NOT NULL,
  `unique_review_id` varchar(100) DEFAULT NULL,
  `review_rating` decimal(2,1) NOT NULL,
  `review_author` varchar(50) NOT NULL,
  `review_text` text NOT NULL,
  `review_url` varchar(300) NOT NULL,
  `review_created` datetime NOT NULL,
  `date_polled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget_id_2` (`widget_id`,`review_id`),
  KEY `widget_id` (`widget_id`),
  KEY `company_id` (`company_id`),
  KEY `review_directory` (`review_directory`),
  KEY `date_polled` (`date_polled`),
  KEY `review_created` (`review_created`),
  KEY `review_rating` (`review_rating`)
) ENGINE=InnoDB AUTO_INCREMENT=1145230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `local_reviews`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `local_reviews` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `review_directory` enum('facebook','google','yelp','yellowpages','tripadvisor') NOT NULL,
  `review_profile_url` varchar(300) NOT NULL,
  `review_profile_count` int(11) NOT NULL,
  `review_profile_rating` decimal(2,1) NOT NULL,
  `review_id` varchar(100) NOT NULL,
  `review_rating` decimal(2,1) NOT NULL,
  `review_author` varchar(50) NOT NULL,
  `review_text` text NOT NULL,
  `review_url` varchar(300) NOT NULL,
  `review_created` datetime NOT NULL,
  `date_polled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget_id_2` (`widget_id`,`review_id`),
  KEY `widget_id` (`widget_id`),
  KEY `company_id` (`company_id`),
  KEY `review_directory` (`review_directory`),
  KEY `date_polled` (`date_polled`),
  KEY `review_created` (`review_created`),
  KEY `review_rating` (`review_rating`)
) ENGINE=InnoDB AUTO_INCREMENT=1520620 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nsd_campaign_daily_activity`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `nsd_campaign_daily_activity` (
  `report_date` datetime NOT NULL,
  `advertiser_id` bigint(20) NOT NULL,
  `advertiser_name` varchar(500) NOT NULL,
  `advertiser_code` varchar(500) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `campaign_name` varchar(500) NOT NULL,
  `campaign_spend` decimal(12,2) NOT NULL,
  `campaign_adjustment` decimal(12,2) NOT NULL,
  `campaign_fees` decimal(12,2) NOT NULL,
  `visits` bigint(20) NOT NULL,
  `impressions` bigint(20) NOT NULL,
  `calls` bigint(20) NOT NULL,
  `emails` bigint(20) NOT NULL,
  `coupons` bigint(20) NOT NULL,
  `web_links` bigint(20) NOT NULL,
  `web_events` bigint(20) NOT NULL,
  `last_updated` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `report_date` (`report_date`,`campaign_id`),
  KEY `report_date_2` (`report_date`),
  KEY `advertiser_id` (`advertiser_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `date_updated` (`date_updated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nsd_campaign_event_detail`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `nsd_campaign_event_detail` (
  `report_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `advertiser_id` bigint(20) NOT NULL,
  `advertiser_name` varchar(500) NOT NULL,
  `advertiser_code` varchar(500) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `campaign_name` varchar(500) NOT NULL,
  `event_type_name` varchar(30) NOT NULL,
  `event_type_id` bigint(20) NOT NULL,
  `event_time` datetime NOT NULL,
  `call_result` varchar(30) NOT NULL,
  `call_target` varchar(30) NOT NULL,
  `call_duration` int(11) NOT NULL,
  `universal_id` int(11) NOT NULL,
  `tracking_code` varchar(100) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `customer_phone` varchar(30) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_address` varchar(200) NOT NULL,
  `customer_city` varchar(50) NOT NULL,
  `customer_state` varchar(30) NOT NULL,
  `customer_zip_code` varchar(30) NOT NULL,
  `customer_country` varchar(30) NOT NULL,
  `email_target` varchar(200) NOT NULL,
  `web_event_id` bigint(20) NOT NULL,
  `web_event_name` varchar(200) NOT NULL,
  `web_event_submitted` varchar(30) NOT NULL,
  `web_event_url` varchar(500) NOT NULL,
  `web_event_refer_url` varchar(500) NOT NULL,
  `call_recording_url` varchar(500) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `event_time_2` (`event_time`,`call_target`,`web_event_id`),
  KEY `report_date` (`report_date`),
  KEY `campaign_id` (`campaign_id`),
  KEY `event_time` (`event_time`),
  KEY `date_updated` (`date_updated`),
  KEY `web_event_submitted` (`web_event_submitted`),
  KEY `event_type_id` (`event_type_id`),
  KEY `web_event_name` (`web_event_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nsd_campaign_summary_activity`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `nsd_campaign_summary_activity` (
  `advertiser_id` bigint(20) NOT NULL,
  `advertiser_name` varchar(200) NOT NULL,
  `advertiser_code` varchar(200) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `campaign_name` varchar(200) NOT NULL,
  `campaign_spend` decimal(12,2) NOT NULL,
  `campaign_adjustment` decimal(12,2) NOT NULL,
  `campaign_fees` decimal(12,2) NOT NULL,
  `visits` bigint(20) NOT NULL,
  `impressions` bigint(20) NOT NULL,
  `calls` bigint(20) NOT NULL,
  `emails` bigint(20) NOT NULL,
  `coupons` bigint(20) NOT NULL,
  `web_links` bigint(20) NOT NULL,
  `web_events` bigint(20) NOT NULL,
  `last_updated` datetime NOT NULL,
  `campaign_real_start_date` datetime NOT NULL,
  `campaign_real_end_date` datetime NOT NULL,
  `campaign_target_duration` int(11) NOT NULL,
  `campaign_budget` decimal(12,2) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `campaign_id` (`campaign_id`),
  KEY `campaign_real_start_date` (`campaign_real_start_date`),
  KEY `campaign_real_end_date` (`campaign_real_end_date`),
  KEY `date_updated` (`date_updated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publisher_accounts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `publisher_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `name` varbinary(500) NOT NULL,
  `oauth_access_token` varbinary(2000) DEFAULT NULL,
  `oauth_refresh_token` varbinary(2000) DEFAULT NULL,
  `oauth_other_info` varbinary(500) DEFAULT NULL,
  `oauth_expires_in` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `oauth_custom_url` varbinary(500) DEFAULT NULL,
  `misc_info` varbinary(5000) DEFAULT NULL,
  `oauth_token_granted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_last_posted` datetime DEFAULT NULL,
  `date_last_polled` datetime DEFAULT NULL,
  `poll_status` enum('idle','queued','processing') NOT NULL DEFAULT 'idle',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `service_type` enum('facebook','google','twitter','linkedin','instagram','pinterest') CHARACTER SET latin1 NOT NULL,
  `service_uid` varchar(30) NOT NULL,
  `service_uname` varchar(50) DEFAULT NULL,
  `service_dname` varchar(100) NOT NULL,
  `service_uthumb` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`company_id`,`name`,`service_type`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `service_type` (`service_type`),
  KEY `date_last_posted` (`date_last_posted`),
  KEY `date_last_polled` (`date_last_polled`),
  KEY `poll_status` (`poll_status`)
) ENGINE=InnoDB AUTO_INCREMENT=40736 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publisher_posts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `publisher_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `post_id` varchar(45) DEFAULT NULL,
  `post_group_id` bigint(20) NOT NULL,
  `post_text` text CHARACTER SET latin1 NOT NULL,
  `post_attachment` varchar(40) DEFAULT NULL,
  `post_service_type` enum('facebook','google','twitter','linkedin','instagram','pinterest') NOT NULL,
  `post_service_uid` varchar(30) NOT NULL,
  `post_service_uname` varchar(50) DEFAULT NULL,
  `post_service_dname` varchar(50) DEFAULT NULL,
  `post_service_uthumb` varchar(250) DEFAULT NULL,
  `post_url` varchar(250) DEFAULT NULL,
  `post_misc_info` varchar(30) DEFAULT NULL,
  `post_status` enum('pending','queued','processing','published','error','error_auth','deleted') CHARACTER SET latin1 NOT NULL,
  `stats_favorites` int(11) NOT NULL DEFAULT '0',
  `stats_retweets` int(11) NOT NULL DEFAULT '0',
  `stats_replies` int(11) NOT NULL DEFAULT '0',
  `stats_likes` int(11) NOT NULL DEFAULT '0',
  `stats_comments` int(11) NOT NULL DEFAULT '0',
  `stats_plusoners` int(11) NOT NULL DEFAULT '0',
  `stats_saves` int(11) NOT NULL DEFAULT '0',
  `stats_resharers` int(11) NOT NULL DEFAULT '0',
  `stats_clicks` int(11) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_published` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `post_group_id` (`post_group_id`),
  KEY `post_status` (`post_status`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `date_created` (`date_created`),
  KEY `date_scheduled` (`date_scheduled`),
  KEY `date_published` (`date_published`),
  KEY `stats_favorites` (`stats_favorites`),
  KEY `stats_retweets` (`stats_retweets`),
  KEY `stats_replies` (`stats_replies`),
  KEY `stats_likes` (`stats_likes`),
  KEY `stats_comments` (`stats_comments`),
  KEY `stats_clicks` (`stats_clicks`),
  KEY `account_id` (`account_id`),
  KEY `stats_plusoners` (`stats_plusoners`),
  KEY `stats_resharers` (`stats_resharers`),
  KEY `widget_id` (`widget_id`),
  KEY `stats_saves` (`stats_saves`),
  KEY `post_service_type` (`post_service_type`),
  KEY `post_id_2` (`post_id`,`post_service_type`)
) ENGINE=InnoDB AUTO_INCREMENT=28073 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shortener_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `shortener_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url_id` int(10) unsigned NOT NULL,
  `referrer` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `ip_address` varchar(41) CHARACTER SET latin1 DEFAULT NULL,
  `country_code` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `date_clicked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `url_id` (`url_id`),
  KEY `date_clicked` (`date_clicked`)
) ENGINE=InnoDB AUTO_INCREMENT=1365883 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shortener_urls`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `shortener_urls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `long_url` varchar(255) NOT NULL,
  `clicks` int(10) unsigned NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `long_url` (`long_url`),
  KEY `clicks` (`clicks`)
) ENGINE=InnoDB AUTO_INCREMENT=22249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_tweets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `twitter_tweets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tweet_id` varchar(20) NOT NULL DEFAULT '',
  `twitter_id` varchar(20) NOT NULL DEFAULT '',
  `user_id` bigint(11) DEFAULT NULL,
  `company_id` bigint(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `favorites` int(11) NOT NULL,
  `replies` int(11) NOT NULL,
  `retweets` int(11) NOT NULL,
  `text` varchar(175) NOT NULL DEFAULT '',
  `twitter_username` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tweet_id_2` (`tweet_id`,`company_id`),
  KEY `tweet_id` (`tweet_id`),
  KEY `twitter_id` (`twitter_id`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `date_created` (`date_created`),
  KEY `favorites` (`favorites`),
  KEY `replies` (`replies`),
  KEY `retweets` (`retweets`),
  KEY `twitter_username` (`twitter_username`),
  KEY `company_id_2` (`company_id`,`date_created`)
) ENGINE=InnoDB AUTO_INCREMENT=3884613 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_tweets_legacy`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `twitter_tweets_legacy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tweet_id` varchar(20) NOT NULL DEFAULT '',
  `twitter_id` varchar(20) NOT NULL DEFAULT '',
  `user_id` bigint(11) DEFAULT NULL,
  `company_id` bigint(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `favorites` int(11) NOT NULL,
  `replies` int(11) NOT NULL,
  `retweets` int(11) NOT NULL,
  `text` varchar(175) NOT NULL DEFAULT '',
  `twitter_username` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `tweet_id` (`tweet_id`),
  KEY `twitter_id` (`twitter_id`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `date_created` (`date_created`),
  KEY `favorites` (`favorites`),
  KEY `replies` (`replies`),
  KEY `retweets` (`retweets`),
  KEY `twitter_username` (`twitter_username`),
  KEY `company_id_2` (`company_id`,`date_created`)
) ENGINE=InnoDB AUTO_INCREMENT=300419036 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `twitter_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) DEFAULT NULL,
  `twitter_id` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `first_tweet_date` datetime DEFAULT NULL,
  `since_id` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `company_id` bigint(11) DEFAULT NULL,
  `twitter_username` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `twitter_id` (`twitter_id`),
  KEY `first_tweet_date` (`first_tweet_date`),
  KEY `since_id` (`since_id`),
  KEY `company_id` (`company_id`),
  KEY `twitter_username` (`twitter_username`)
) ENGINE=InnoDB AUTO_INCREMENT=26012 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'cyfe'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 21:07:19
-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: cyfe-db-1.ctjgy3dx7shp.us-east-1.rds.amazonaws.com    Database: cyfe
-- ------------------------------------------------------
-- Server version	5.5.53-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS cyfe_db_test2;
USE cyfe_db_test;

-- cyfe.alerts definition

CREATE TABLE IF NOT EXISTS `alerts` (
  `to` varchar(100) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.api_embed_erros definition

CREATE TABLE IF NOT EXISTS `api_embed_erros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` varchar(25) DEFAULT NULL,
  `api_embed_secret` varchar(50) DEFAULT NULL,
  `api_embed_key` varchar(50) DEFAULT NULL,
  `auth` varchar(1000) DEFAULT NULL,
  `uid` varchar(25) DEFAULT NULL,
  `ts` varchar(25) DEFAULT NULL,
  `access` varchar(15) DEFAULT NULL,
  `signature` varchar(1000) DEFAULT NULL,
  `original_cookie` varchar(250) DEFAULT NULL,
  `new_cookie` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.bitly_widgets definition

CREATE TABLE IF NOT EXISTS `bitly_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` int(11) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `group_id` varchar(100) DEFAULT NULL,
  `bitly_link` varchar(100) DEFAULT NULL,
  `metric` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;


-- cyfe.cache definition

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.clients_sharing definition

CREATE TABLE IF NOT EXISTS `clients_sharing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `date_shared` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`client_id`),
  KEY `date_shared` (`date_shared`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114268 DEFAULT CHARSET=latin1;


-- cyfe.companies definition

CREATE TABLE IF NOT EXISTS `companies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `partner_company_id` bigint(20) DEFAULT NULL,
  `partner_min_clients` smallint(6) DEFAULT NULL,
  `referral_company_id` bigint(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `suspended` datetime DEFAULT NULL,
  `plan_id` smallint(6) NOT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `recurly_subscription_id` varchar(255) DEFAULT NULL,
  `recurly_invoices_pulled` datetime DEFAULT NULL,
  `recurly_coupon_code` varchar(50) DEFAULT NULL,
  `branding_logo` varchar(10) DEFAULT NULL,
  `branding_domain` varchar(80) DEFAULT NULL,
  `has_branding_domain` tinyint(1) NOT NULL DEFAULT '0',
  `branding_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `branding_email` varchar(75) DEFAULT NULL,
  `branding_link` varchar(255) DEFAULT NULL,
  `branding_css` tinyint(1) DEFAULT NULL,
  `beta_branding_css` tinyint(1) DEFAULT NULL,
  `branding_theme` tinyint(1) DEFAULT NULL,
  `branding_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `branding_widgets` tinyint(1) NOT NULL DEFAULT '0',
  `branding_compare` enum('NONE','PERIOD','YEAR') NOT NULL DEFAULT 'PERIOD',
  `branding_yaxis` tinyint(1) NOT NULL DEFAULT '0',
  `branding_adwordsmargin` varchar(10) DEFAULT NULL,
  `branding_bingads_margin` varchar(10) DEFAULT NULL,
  `branding_facebookads_margin` varchar(10) DEFAULT NULL,
  `branding_customkey` varchar(64) DEFAULT NULL,
  `branding_email_export_message` varchar(300) DEFAULT NULL,
  `branding_email_account_creation` varchar(300) DEFAULT NULL,
  `branding_email_edit_user` varchar(300) DEFAULT NULL,
  `branding_email_password_reset` varchar(300) DEFAULT NULL,
  `branding_email_expired_widget` varchar(650) DEFAULT NULL,
  `branding_email_widget_alert` varchar(300) DEFAULT NULL,
  `api_embed_key` varchar(32) DEFAULT NULL,
  `api_embed_secret` varchar(32) DEFAULT NULL,
  `api_embed_uid` varchar(32) DEFAULT NULL,
  `first_subscribed` datetime DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `source` varchar(500) DEFAULT NULL,
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_billing_info_added` datetime DEFAULT NULL,
  `date_first_charged` datetime DEFAULT NULL,
  `last_exported` datetime DEFAULT NULL,
  `daily_exports_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `paid_dashboards` smallint(6) NOT NULL DEFAULT '0',
  `legacy_paid_dashboards` smallint(6) NOT NULL DEFAULT '0',
  `pricing_change_discount` tinyint(1) NOT NULL DEFAULT '0',
  `yearly_one_month` tinyint(1) DEFAULT NULL,
  `yearly_eleven_months` tinyint(1) DEFAULT NULL,
  `mass_pricing_change` tinyint(1) NOT NULL DEFAULT '0',
  `failed_change` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `successful_change` tinyint(1) NOT NULL DEFAULT '0',
  `starter_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `company_type` varchar(50) DEFAULT NULL,
  `company_industry` varchar(50) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `free_extra_widgets` smallint(6) DEFAULT NULL,
  `ga_id` varchar(100) DEFAULT NULL,
  `dashboard_templates` tinyint(1) NOT NULL DEFAULT '0',
  `trial` int(4) NOT NULL DEFAULT '0',
  `show_widget_update_time` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_embed_key` (`api_embed_key`),
  UNIQUE KEY `partner_company_id_2` (`partner_company_id`,`api_embed_uid`),
  KEY `active` (`active`),
  KEY `plan_id` (`plan_id`),
  KEY `payment_method` (`payment_method`),
  KEY `recurly_invoices_pulled` (`recurly_invoices_pulled`),
  KEY `recurly_subscription_id` (`recurly_subscription_id`),
  KEY `date_registered` (`date_registered`),
  KEY `suspended` (`suspended`),
  KEY `first_subscribed` (`first_subscribed`),
  KEY `branding_domain` (`branding_domain`),
  KEY `branding_name` (`branding_name`),
  KEY `branding_email` (`branding_email`),
  KEY `partner_company_id` (`partner_company_id`),
  KEY `referral_company_id` (`referral_company_id`),
  KEY `ip` (`ip`),
  KEY `source` (`source`),
  KEY `branding_css` (`branding_css`),
  KEY `api_embed_uid` (`api_embed_uid`),
  KEY `companies_branding_currency_IDX` (`branding_currency`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1000066677 DEFAULT CHARSET=latin1;


-- cyfe.custom_css definition

CREATE TABLE IF NOT EXISTS `custom_css` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_company_id` int(11) NOT NULL,
  `dashboard_bg_color` varchar(11) DEFAULT NULL,
  `button_bg_color` varchar(11) DEFAULT NULL,
  `dd_tf_bg_color` varchar(11) DEFAULT NULL,
  `content_bg_color` varchar(11) DEFAULT NULL,
  `content_font_style` varchar(20) DEFAULT NULL,
  `content_font_color` varchar(11) DEFAULT NULL,
  `content_font_size` varchar(20) DEFAULT NULL,
  `button_font_style` varchar(20) DEFAULT NULL,
  `button_font_color` varchar(11) DEFAULT NULL,
  `button_font_size` varchar(20) DEFAULT NULL,
  `dd_tf_font_style` varchar(20) DEFAULT NULL,
  `dd_tf_font_color` varchar(11) DEFAULT NULL,
  `dd_tf_font_size` varchar(20) DEFAULT NULL,
  `chart_colors` varchar(100) DEFAULT NULL,
  `chart_color1` varchar(12) DEFAULT NULL,
  `chart_color2` varchar(12) DEFAULT NULL,
  `chart_color3` varchar(12) DEFAULT NULL,
  `chart_color4` varchar(12) DEFAULT NULL,
  `chart_color5` varchar(12) DEFAULT NULL,
  `chart_color6` varchar(12) DEFAULT NULL,
  `chart_color7` varchar(12) DEFAULT NULL,
  `chart_color8` varchar(12) DEFAULT NULL,
  `chart_color9` varchar(12) DEFAULT NULL,
  `chart_color10` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12883 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_access definition

CREATE TABLE IF NOT EXISTS `dashboard_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `sharing_with` varchar(255) NOT NULL,
  `hash` varchar(28) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date_last_accessed` datetime DEFAULT NULL,
  `date_given` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dashboard_resize` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dashboard_id_2` (`dashboard_id`,`hash`),
  KEY `date_given` (`date_given`),
  KEY `dashboard_id` (`dashboard_id`),
  KEY `hash` (`hash`),
  KEY `date_last_accessed` (`date_last_accessed`),
  KEY `company_id` (`company_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=217993 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_accounts definition

CREATE TABLE IF NOT EXISTS `dashboard_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `name` varbinary(500) NOT NULL,
  `oauth_access_token` varbinary(2000) DEFAULT NULL,
  `oauth_refresh_token` varbinary(2000) DEFAULT NULL,
  `oauth_other_info` varbinary(500) DEFAULT NULL,
  `oauth_expires_in` varchar(20) DEFAULT NULL,
  `oauth_custom_url` varbinary(500) DEFAULT NULL,
  `misc_info` varbinary(5000) DEFAULT NULL,
  `oauth_token_granted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`company_id`,`service_id`,`name`),
  KEY `company_id` (`company_id`),
  KEY `service_id` (`service_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1070039 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_notices definition

CREATE TABLE IF NOT EXISTS `dashboard_notices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'info',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `free_plan_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_notices_users definition

CREATE TABLE IF NOT EXISTS `dashboard_notices_users` (
  `dashboard_notice_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.dashboard_positions definition

CREATE TABLE IF NOT EXISTS `dashboard_positions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `positions` varchar(7500) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`dashboard_id`),
  KEY `positions` (`positions`(767)),
  KEY `company_id` (`company_id`),
  KEY `dashboard_id` (`dashboard_id`)
) ENGINE=InnoDB AUTO_INCREMENT=804128 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_services definition

CREATE TABLE IF NOT EXISTS `dashboard_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `service` varchar(255) NOT NULL,
  `footer_link` varchar(255) DEFAULT NULL,
  `refresh_rate` int(11) NOT NULL,
  `default_height` int(11) NOT NULL,
  `production` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `whitelabel` tinyint(1) NOT NULL DEFAULT '1',
  `for_company_id` bigint(20) DEFAULT NULL,
  `free_limit` int(11) NOT NULL DEFAULT '100',
  `premium_limit` int(11) NOT NULL DEFAULT '100',
  `schedule_interval` int(11) NOT NULL DEFAULT '0',
  `data_source_field_label` varchar(255) NOT NULL,
  `data_source_type` enum('URL','OAUTH','OAUTH2','API','DATETIME','TIMEZONE','CHART','FILE','PAYPAL','TEXT','CUSTOM') NOT NULL,
  `data_source_desc` varchar(500) DEFAULT NULL,
  `data_source_historical` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label2` varchar(255) NOT NULL,
  `data_source_type2` enum('OAUTH2','PASSWORD','URL','FILE','API','HOORAY','PINTEREST','SALESFORCE','PUSH_API','CUSTOM','FLICKR','DESK','ZENDESK','SHOPIFY','QUICKBOOKS','XERO','INSTAGRAM') NOT NULL,
  `data_source_desc2` varchar(600) DEFAULT NULL,
  `data_source_historical2` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label3` varchar(255) NOT NULL,
  `data_source_type3` enum('API','REFRESH','AWS_CW','CHART','TIMEFORMAT','GA','YOUTUBE','FACEBOOK_PAGES','TWITTER','SERPS_ENGINES','INSTAGRAM','ADWORDS','CUSTOM','GART','GWT','GPLUS','OAUTH2','FACEBOOK_AD_CAMPAIGNS','QUICKBOOKS') NOT NULL DEFAULT 'API',
  `data_source_desc3` varchar(255) DEFAULT NULL,
  `data_source_historical3` int(11) NOT NULL DEFAULT '0',
  `data_source_field_label4` varchar(255) NOT NULL,
  `data_source_type4` enum('API','AWS_REGIONS','GA_SEGMENTS','ADWORDS_MCC_CAMPAIGNS','AWEBER_CAMPAIGNS','CAMPAIGN_MONITOR','GOOGLE_DFP','ITUNES_CONNECT','CUSTOM','BASECAMP_PROJECTS','BASECAMP_CLASSIC_PROJECTS','VIMEO_VIDEOS','UNBOUNCE_PAGES','BINGADS_CAMPAIGNS','FACEBOOK_ACTION_TYPES') NOT NULL,
  `data_source_desc4` varchar(255) DEFAULT NULL,
  `data_source_historical4` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label5` varchar(255) NOT NULL,
  `data_source_type5` enum('CUSTOM','GDFP','BASECAMP','BASECAMP_CLASSIC','VIMEO','UNBOUNCE','FACEBOOK_ADS','EVENTBRITE','HIGHRISE','BINGADS') NOT NULL,
  `data_source_desc5` varchar(255) DEFAULT NULL,
  `data_source_historical5` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label6` varchar(255) NOT NULL,
  `data_source_type6` enum('CUSTOM') NOT NULL,
  `data_source_desc6` varchar(255) DEFAULT NULL,
  `data_source_historical6` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label7` varchar(255) NOT NULL,
  `data_source_type7` enum('CUSTOM') NOT NULL,
  `data_source_desc7` varchar(255) DEFAULT NULL,
  `data_source_historical7` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label8` varchar(255) NOT NULL,
  `data_source_type8` enum('CUSTOM') NOT NULL,
  `data_source_desc8` varchar(255) DEFAULT NULL,
  `data_source_historical8` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label9` varchar(255) NOT NULL,
  `data_source_type9` enum('CUSTOM') NOT NULL,
  `data_source_desc9` varchar(255) DEFAULT NULL,
  `data_source_historical9` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label10` varchar(255) NOT NULL,
  `data_source_type10` enum('CUSTOM') NOT NULL,
  `data_source_desc10` varchar(255) DEFAULT NULL,
  `data_source_historical10` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label11` varchar(255) NOT NULL,
  `data_source_type11` enum('CUSTOM') NOT NULL,
  `data_source_desc11` varchar(255) DEFAULT NULL,
  `data_source_historical11` tinyint(1) NOT NULL DEFAULT '0',
  `data_source_field_label12` varchar(255) NOT NULL,
  `data_source_type12` enum('CUSTOM') NOT NULL,
  `data_source_desc12` varchar(255) DEFAULT NULL,
  `data_source_historical12` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `oauth_request` varchar(500) DEFAULT NULL,
  `oauth_authenticate` varchar(500) DEFAULT NULL,
  `oauth_token` varchar(500) DEFAULT NULL,
  `oauth_refresh` varchar(500) DEFAULT NULL,
  `oauth_account` varchar(500) DEFAULT NULL,
  `oauth_account2` varchar(500) DEFAULT NULL,
  `oauth_account3` varchar(500) DEFAULT NULL,
  `oauth_account4` varchar(500) DEFAULT NULL,
  `oauth_other_info_field` varchar(50) DEFAULT NULL,
  `oauth_custom_url_label` varchar(50) DEFAULT NULL,
  `oauth_custom_url_desc` varchar(200) DEFAULT NULL,
  `db_table_name` varchar(60) DEFAULT NULL,
  `class_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `refresh_rate` (`refresh_rate`),
  KEY `service` (`service`),
  KEY `description` (`description`),
  KEY `category` (`category`),
  KEY `production` (`production`),
  KEY `whitelabel` (`whitelabel`),
  KEY `service_2` (`service`,`description`),
  KEY `for_company_id` (`for_company_id`),
  KEY `featured` (`featured`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_services_wl definition

CREATE TABLE IF NOT EXISTS `dashboard_services_wl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `additional_widgets` smallint(6) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`,`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=438 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_sharing definition

CREATE TABLE IF NOT EXISTS `dashboard_sharing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `type` enum('ADMIN_USER','READ_USER') NOT NULL DEFAULT 'READ_USER',
  `date_shared` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dashboard_id` (`dashboard_id`,`user_id`),
  KEY `company_id` (`company_id`),
  KEY `date_shared` (`date_shared`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2471702 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_widgets definition

CREATE TABLE IF NOT EXISTS `dashboard_widgets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `widget_title` varchar(255) NOT NULL,
  `data_source` varbinary(1500) NOT NULL,
  `data_source2` varbinary(750) NOT NULL,
  `data_source3` varbinary(500) NOT NULL,
  `data_source4` varbinary(4500) NOT NULL DEFAULT '',
  `data_source5` varbinary(1000) NOT NULL,
  `data_source6` varbinary(500) NOT NULL,
  `data_source7` varbinary(500) NOT NULL,
  `data_source8` varbinary(500) NOT NULL,
  `data_source9` varbinary(500) NOT NULL,
  `data_source10` varbinary(500) NOT NULL,
  `data_source11` varbinary(500) NOT NULL,
  `data_source12` varbinary(500) NOT NULL,
  `data_source13` varbinary(500) NOT NULL,
  `data_source14` varbinary(500) NOT NULL,
  `data_source15` varbinary(500) NOT NULL,
  `data_source16` varbinary(500) NOT NULL,
  `widget_goals` varbinary(500) DEFAULT NULL,
  `widget_drilldown` varbinary(500) DEFAULT NULL,
  `widget_drilldown_external` tinyint(1) DEFAULT NULL,
  `data_config` varbinary(500) DEFAULT NULL,
  `custom_refresh_rate` int(11) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_refreshed` timestamp NULL DEFAULT NULL,
  `starter_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `widget_configured` tinyint(1) NOT NULL DEFAULT '0',
  `template_widget` tinyint(1) NOT NULL DEFAULT '0',
  `template_wid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dashboard_id` (`dashboard_id`),
  KEY `widget_title` (`widget_title`),
  KEY `date_added` (`date_added`),
  KEY `company_id` (`company_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6588277 DEFAULT CHARSET=latin1;


-- cyfe.dashboard_widgets_serps definition

CREATE TABLE IF NOT EXISTS `dashboard_widgets_serps` (
  `widget_id` bigint(20) NOT NULL,
  `keyword_id` varchar(50) NOT NULL,
  `phrase` varchar(500) NOT NULL,
  `engine` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `widget_id` (`widget_id`),
  KEY `keyword_id` (`keyword_id`),
  KEY `phrase` (`phrase`),
  KEY `engine` (`engine`),
  KEY `date_added` (`date_added`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.dashboards definition

CREATE TABLE IF NOT EXISTS `dashboards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `icon_id` smallint(6) NOT NULL,
  `background` varchar(10) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `first_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `date_added` (`date_added`),
  KEY `title` (`title`),
  KEY `company_id` (`company_id`),
  KEY `sort_order` (`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=972156 DEFAULT CHARSET=latin1;


-- cyfe.deactivated_account_login_attempts definition

CREATE TABLE IF NOT EXISTS `deactivated_account_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159071 DEFAULT CHARSET=utf8;


-- cyfe.emails definition

CREATE TABLE IF NOT EXISTS `emails` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `from_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `to_user_id` bigint(20) DEFAULT NULL,
  `to_company_id` bigint(20) DEFAULT NULL,
  `subject` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `message_text` text COLLATE utf8_unicode_ci NOT NULL,
  `message_html` text COLLATE utf8_unicode_ci,
  `send_error` text COLLATE utf8_unicode_ci,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `to_user_id` (`to_user_id`),
  KEY `date_created` (`date_created`),
  KEY `date_sent` (`date_sent`),
  KEY `to_company_id` (`to_company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3880668 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- cyfe.emails_ars definition

CREATE TABLE IF NOT EXISTS `emails_ars` (
  `id` smallint(20) NOT NULL AUTO_INCREMENT,
  `query` varchar(1000) NOT NULL,
  `send_order` smallint(6) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `subject` varchar(500) NOT NULL,
  `from_name` varchar(100) NOT NULL,
  `from_address` varchar(100) NOT NULL,
  `message_text` text NOT NULL,
  `message_html` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `date_added` (`date_added`),
  KEY `send_order` (`send_order`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;


-- cyfe.emails_ars_log definition

CREATE TABLE IF NOT EXISTS `emails_ars_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `responder_id` smallint(6) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `responder_id` (`responder_id`),
  KEY `date_sent` (`date_sent`)
) ENGINE=InnoDB AUTO_INCREMENT=1046194 DEFAULT CHARSET=latin1;


-- cyfe.errors definition

CREATE TABLE IF NOT EXISTS `errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `host` varchar(100) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `error` text NOT NULL,
  `post` text NOT NULL,
  `get` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `host` (`host`),
  KEY `uri` (`uri`),
  KEY `ip` (`ip`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=44890 DEFAULT CHARSET=latin1;


-- cyfe.errors_api definition

CREATE TABLE IF NOT EXISTS `errors_api` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `service_id` int(11) NOT NULL,
  `host` varchar(100) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `status` varchar(50) NOT NULL,
  `endpoint` varchar(500) NOT NULL,
  `request` text NOT NULL,
  `response` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `service_id` (`service_id`),
  KEY `status` (`status`),
  KEY `endpoint` (`endpoint`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1054007 DEFAULT CHARSET=latin1;


-- cyfe.failed_jobs definition

CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.features definition

CREATE TABLE IF NOT EXISTS `features` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `features_feature_unique` (`feature`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.gatherup_widgets definition

CREATE TABLE IF NOT EXISTS `gatherup_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` int(11) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `metric` varchar(100) DEFAULT NULL,
  `chart_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;


-- cyfe.google_analytics_widgets definition

CREATE TABLE IF NOT EXISTS `google_analytics_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` int(11) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `campaign` varchar(100) DEFAULT NULL,
  `metric` varchar(50) DEFAULT NULL,
  `segment` varchar(100) DEFAULT NULL,
  `goal` varchar(100) DEFAULT NULL,
  `filters` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33477 DEFAULT CHARSET=utf8;


-- cyfe.gradeus_widgets definition

CREATE TABLE IF NOT EXISTS `gradeus_widgets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL DEFAULT '156',
  `review_url1` varbinary(500) NOT NULL DEFAULT '',
  `review_url2` varbinary(500) NOT NULL DEFAULT '',
  `review_url3` varbinary(500) NOT NULL DEFAULT '',
  `review_url4` varbinary(4000) NOT NULL DEFAULT '',
  `review_url5` varbinary(1000) NOT NULL DEFAULT '',
  `review_type1` varbinary(500) DEFAULT NULL,
  `review_type2` varbinary(500) DEFAULT NULL,
  `review_type3` varbinary(500) DEFAULT NULL,
  `review_type4` varbinary(500) DEFAULT NULL,
  `review_type5` varbinary(500) DEFAULT NULL,
  `site_id1` varbinary(500) DEFAULT NULL,
  `site_id2` varbinary(500) DEFAULT NULL,
  `site_id3` varbinary(500) DEFAULT NULL,
  `site_id4` varbinary(500) DEFAULT NULL,
  `site_id5` varbinary(500) DEFAULT NULL,
  `poll_status` enum('INACTIVE','PENDING','ACTIVE','FAILED') NOT NULL DEFAULT 'INACTIVE',
  `metric` varchar(100) DEFAULT NULL,
  `last_polled_timestamp` datetime NOT NULL DEFAULT '2015-01-01 12:00:00',
  PRIMARY KEY (`id`),
  KEY `dashboard_id` (`dashboard_id`),
  KEY `company_id` (`company_id`),
  KEY `service_id` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27005 DEFAULT CHARSET=latin1;


-- cyfe.instagram_widgets definition

CREATE TABLE IF NOT EXISTS `instagram_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` bigint(20) NOT NULL,
  `dashboard_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `service_id` int(11) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `fb_page_id` varchar(100) DEFAULT NULL,
  `instagram_id` varchar(100) DEFAULT NULL,
  `metric` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9814 DEFAULT CHARSET=utf8;


-- cyfe.invoices definition

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `recurly_invoice_id` bigint(20) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id_2` (`company_id`,`recurly_invoice_id`),
  KEY `amount` (`amount`),
  KEY `company_id` (`company_id`),
  KEY `date_created` (`date_created`),
  KEY `recurly_invoice_id` (`recurly_invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1195954 DEFAULT CHARSET=latin1;


-- cyfe.migrations definition

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.partners_pending definition

CREATE TABLE IF NOT EXISTS `partners_pending` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  `partner_min_clients` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=1183 DEFAULT CHARSET=latin1;


-- cyfe.password_resets definition

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.plans definition

CREATE TABLE IF NOT EXISTS `plans` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `period` varchar(10) DEFAULT NULL,
  `dashboards` mediumint(6) NOT NULL,
  `users` mediumint(8) unsigned DEFAULT NULL,
  `widgets` mediumint(6) NOT NULL,
  `branding_logo` tinyint(1) NOT NULL DEFAULT '1',
  `branding_name` tinyint(1) NOT NULL DEFAULT '0',
  `branding_domain` tinyint(1) NOT NULL DEFAULT '0',
  `branding_email` tinyint(1) NOT NULL DEFAULT '0',
  `branding_link` tinyint(4) NOT NULL,
  `branding_css` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL,
  `partner` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `active` (`active`),
  KEY `partner` (`partner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.promo_amazon_cards definition

CREATE TABLE IF NOT EXISTS `promo_amazon_cards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `for_user_id` bigint(20) NOT NULL,
  `for_company_id` bigint(20) NOT NULL,
  `claim_code` varchar(100) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `response` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `for_company_id` (`for_company_id`),
  KEY `for_user_id` (`for_user_id`),
  KEY `claim_code` (`claim_code`),
  KEY `amount` (`amount`),
  KEY `date_created` (`date_created`),
  KEY `date_updated` (`date_updated`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=latin1;


-- cyfe.public_api_keys definition

CREATE TABLE IF NOT EXISTS `public_api_keys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT NULL,
  `date_of_creation` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `date_last_used` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


-- cyfe.schedules definition

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `dashboard_id` bigint(20) DEFAULT NULL,
  `widget_id` bigint(20) DEFAULT NULL,
  `interval` int(11) NOT NULL,
  `function` varchar(50) NOT NULL,
  `data` text NOT NULL,
  `status` enum('IDLE','QUEUE') NOT NULL DEFAULT 'IDLE',
  `date_queued` datetime DEFAULT NULL,
  `date_success` datetime DEFAULT NULL,
  `date_processed` datetime DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `widget_id_2` (`widget_id`,`function`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `interval` (`interval`),
  KEY `dashboard_id` (`dashboard_id`),
  KEY `function` (`function`),
  KEY `date_success` (`date_success`),
  KEY `date_queued` (`date_queued`),
  KEY `status` (`status`),
  KEY `date_processed` (`date_processed`),
  KEY `widget_id` (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=859679 DEFAULT CHARSET=latin1;


-- cyfe.segment_user definition

CREATE TABLE IF NOT EXISTS `segment_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `segment_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.segments definition

CREATE TABLE IF NOT EXISTS `segments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.sessions definition

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(128) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `data` longtext NOT NULL,
  `ip` varchar(50) NOT NULL,
  `ua` varchar(300) NOT NULL,
  `first_accessed` datetime NOT NULL,
  `last_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `last_accessed` (`last_accessed`),
  KEY `first_accessed` (`first_accessed`),
  KEY `ip` (`ip`),
  KEY `ua` (`ua`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.ssl_certificates definition

CREATE TABLE IF NOT EXISTS `ssl_certificates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reserved` tinyint(1) NOT NULL DEFAULT '0',
  `verification_filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_issued` datetime DEFAULT NULL,
  `date_expiring` datetime DEFAULT NULL,
  `date_processing` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`) USING BTREE,
  KEY `date_added` (`date_added`),
  KEY `date_issued` (`date_issued`),
  KEY `date_expiring` (`date_expiring`),
  KEY `error` (`error`),
  KEY `reserved` (`reserved`),
  KEY `date_processing` (`date_processing`)
) ENGINE=InnoDB AUTO_INCREMENT=600233 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.stats_user_invites definition

CREATE TABLE IF NOT EXISTS `stats_user_invites` (
  `user_id` bigint(20) NOT NULL,
  `invites` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `user_id` (`user_id`),
  KEY `invites` (`invites`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.test_input definition

CREATE TABLE IF NOT EXISTS `test_input` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


-- cyfe.texts definition

CREATE TABLE IF NOT EXISTS `texts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `for_user_id` bigint(20) DEFAULT NULL,
  `for_company_id` bigint(20) DEFAULT NULL,
  `message_text` text COLLATE utf8_unicode_ci NOT NULL,
  `message_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `send_error` text COLLATE utf8_unicode_ci,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `for_user_id` (`for_user_id`),
  KEY `date_created` (`date_created`),
  KEY `date_sent` (`date_sent`),
  KEY `for_company_id` (`for_company_id`),
  KEY `phone_number` (`phone_number`),
  KEY `message_id` (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33654 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- cyfe.tmp_oauth definition

CREATE TABLE IF NOT EXISTS `tmp_oauth` (
  `dwid` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `token_secret` varchar(500) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `dwid` (`dwid`),
  KEY `timestamp` (`timestamp`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.tmp_oauth_urls definition

CREATE TABLE IF NOT EXISTS `tmp_oauth_urls` (
  `dwid` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `custom_url` varchar(500) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `dwid` (`dwid`),
  KEY `company_id` (`company_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- cyfe.users definition

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('ADMIN','READ') NOT NULL DEFAULT 'ADMIN',
  `theme` enum('DARK','LIGHT') NOT NULL DEFAULT 'LIGHT',
  `timezone` varchar(50) DEFAULT NULL,
  `sitetran_language` varchar(2) DEFAULT NULL,
  `ars_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `logins` bigint(20) NOT NULL DEFAULT '0',
  `date_login` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `failed_logins` int(11) NOT NULL DEFAULT '0',
  `last_failed_login` datetime DEFAULT NULL,
  `last_password_reset` datetime DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `show_hidden_widgets` tinyint(1) NOT NULL DEFAULT '0',
  `marketing_trial` tinyint(1) NOT NULL DEFAULT '0',
  `reset_token` varchar(100) DEFAULT NULL,
  `reset_token_create_date` timestamp NULL DEFAULT NULL,
  `two_factor_secret` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `password` (`password`),
  KEY `date_registered` (`date_registered`),
  KEY `active` (`active`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `company_id` (`company_id`),
  KEY `date_login` (`date_login`),
  KEY `logins` (`logins`),
  KEY `type` (`type`),
  KEY `ars_subscription` (`ars_subscription`)
) ENGINE=InnoDB AUTO_INCREMENT=504542 DEFAULT CHARSET=latin1;


-- cyfe.users_cancelled definition

CREATE TABLE IF NOT EXISTS `users_cancelled` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `plan_id` bigint(20) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `explanation` text,
  `source` varchar(500) DEFAULT NULL,
  `first_subscribed` datetime DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `date_cancelled` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_billing_info_added` datetime DEFAULT NULL,
  `date_first_charged` datetime DEFAULT NULL,
  `platform` varchar(75) DEFAULT NULL,
  `trial` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `reason` (`reason`),
  KEY `date_registered` (`date_registered`),
  KEY `date_cancelled` (`date_cancelled`),
  KEY `plan_id` (`plan_id`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `first_subscribed` (`first_subscribed`),
  KEY `source` (`source`)
) ENGINE=InnoDB AUTO_INCREMENT=51510 DEFAULT CHARSET=latin1;


-- cyfe.users_invite_log definition

CREATE TABLE IF NOT EXISTS `users_invite_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `invite_type` varchar(50) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `invite_type` (`invite_type`),
  KEY `date_sent` (`date_sent`)
) ENGINE=InnoDB AUTO_INCREMENT=7315 DEFAULT CHARSET=latin1;


-- cyfe.users_pending definition

CREATE TABLE IF NOT EXISTS `users_pending` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmation` varchar(255) DEFAULT '',
  `date_registered` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `company_type` varchar(100) DEFAULT NULL,
  `company_industry` varchar(100) DEFAULT NULL,
  `ga_id` varchar(100) DEFAULT NULL,
  `plan_id` int(10) NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `confirmation` (`confirmation`),
  KEY `date_registered` (`date_registered`),
  KEY `date_updated` (`date_updated`)
) ENGINE=InnoDB AUTO_INCREMENT=426105 DEFAULT CHARSET=latin1;


-- cyfe.vanity_domains definition

CREATE TABLE IF NOT EXISTS `vanity_domains` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.vanity_urls definition

CREATE TABLE IF NOT EXISTS `vanity_urls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `from` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dashboard_access_id` bigint(20) DEFAULT NULL,
  `vanity_domain_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- cyfe.whitelisted_domains definition

CREATE TABLE IF NOT EXISTS `whitelisted_domains` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `child_src` varchar(128) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `use_sec_policy` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;



--
-- Dumping routines for database 'cyfe'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 20:44:02
INSERT INTO plans (id,name,price,period,dashboards,users,widgets,branding_logo,branding_name,branding_domain,branding_email,branding_link,branding_css,active,partner,date_created) VALUES 
(0,'Client',0.00,'month',9999,NULL,99999,0,0,1,0,0,0,0,0,'2012-03-07 04:00:08.0')
,(1,'Free',0.00,'month',5,NULL,5,1,0,1,0,0,0,1,0,'2011-10-09 23:20:27.0')
,(2,'Basic',9.00,'month',9999,NULL,20,1,0,0,0,0,0,1,0,'2011-10-09 23:20:27.0')
,(3,'Standard',29.00,'month',9999,NULL,50,1,0,1,0,0,0,1,0,'2011-10-09 23:20:27.0')
,(5,'Pro',49.00,'month',9999,NULL,100,1,0,1,0,0,0,1,0,'2011-11-09 04:06:24.0')
,(10,'Premium',19.00,'month',9999,NULL,99999,1,0,1,0,0,0,0,0,'2012-10-29 17:02:38.0')
,(11,'Premium (Yearly)',168.00,'year',9999,NULL,99999,1,0,1,0,0,0,0,0,'2012-11-10 21:49:19.0')
,(20,'Premium',19.00,'month',9999,NULL,99999,1,0,1,0,0,0,0,0,'2017-11-11 19:46:00.0')
,(21,'Premium (Yearly)',168.00,'year',9999,NULL,99999,1,0,1,0,0,0,0,0,'2017-11-11 19:46:17.0')
,(22,'Premium (Yearly)',228.00,'year',9999,NULL,99999,1,0,1,0,0,0,0,0,'2018-02-15 20:59:17.0')
;
INSERT INTO plans (id,name,price,period,dashboards,users,widgets,branding_logo,branding_name,branding_domain,branding_email,branding_link,branding_css,active,partner,date_created) VALUES 
(30,'Premium',29.00,'month',9999,NULL,99999,1,0,1,0,0,0,0,0,'2018-08-29 03:30:15.0')
,(31,'Premium (Yearly)',290.00,'year',9999,NULL,99999,1,0,1,0,0,0,0,0,'2018-08-29 03:31:39.0')
,(32,'Premium (Yearly)',348.00,'year',9999,NULL,99999,1,0,1,0,0,0,0,0,'2018-08-29 04:23:21.0')
,(100,'Free',0.00,'month',2,1,5,1,0,1,0,0,1,1,0,'2019-04-16 12:31:18.0')
,(101,'Solo',29.00,'month',5,1,9999,1,0,1,0,0,0,1,0,'2019-04-16 14:30:20.0')
,(102,'Pro',49.00,'month',10,5,9999,1,0,1,0,0,1,1,0,'2019-04-16 15:30:20.0')
,(103,'Premier',89.00,'month',20,NULL,9999,1,0,1,0,0,1,1,0,'2019-04-17 15:30:20.0')
,(201,'Solo (Yearly)',313.00,'year',5,1,9999,1,0,1,0,0,0,1,0,'2019-06-06 17:00:00.0')
,(202,'Pro (Yearly)',529.00,'year',10,5,9999,1,0,1,0,0,1,1,0,'2019-06-06 15:30:20.0')
,(203,'Premier (Yearly)',961.00,'year',20,NULL,9999,1,0,1,0,0,1,1,0,'2019-06-06 15:30:20.0')
;
INSERT INTO plans (id,name,price,period,dashboards,users,widgets,branding_logo,branding_name,branding_domain,branding_email,branding_link,branding_css,active,partner,date_created) VALUES 
(700,'Standard',29.00,'month',9999,NULL,50,1,1,1,1,0,0,0,0,'2012-03-10 20:47:01.0')
,(800,'Trial',0.00,'month',9999,NULL,9999,1,0,1,0,0,0,0,0,'2012-08-10 01:37:11.0')
,(900,'Unlimited',0.00,'month',9999,NULL,9999,1,0,1,0,0,0,0,0,'2012-03-09 05:31:58.0')
,(940,'White Label',49.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2012-11-10 22:42:51.0')
,(950,'White Label',99.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2012-07-17 04:05:39.0')
,(951,'White Label (Yearly)',888.00,'year',9999,NULL,9999,1,1,1,1,1,0,0,1,'2012-12-26 19:10:01.0')
,(1000,'Partner',10.00,'month',9999,NULL,99999,1,1,1,1,1,0,0,1,'2012-03-07 03:57:18.0')
,(1001,'Partner',10.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2012-03-07 03:57:18.0')
,(1010,'Partner',1.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2014-05-21 21:36:58.0')
,(1015,'Partner',15.00,'month',9999,NULL,32767,1,1,1,1,1,0,0,1,'2019-10-23 08:36:48.0')
;
INSERT INTO plans (id,name,price,period,dashboards,users,widgets,branding_logo,branding_name,branding_domain,branding_email,branding_link,branding_css,active,partner,date_created) VALUES 
(1020,'Partner',20.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2016-09-30 22:57:22.0')
,(1021,'Partner',20.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2016-09-30 22:57:22.0')
,(1025,'Partner',2.50,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2013-12-03 14:37:59.0')
,(1030,'Partner',3.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2014-08-15 17:00:22.0')
,(1050,'Partner',5.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2013-02-04 20:48:23.0')
,(1051,'Partner (Yearly)',45.00,'year',9999,NULL,9999,1,1,1,1,1,0,0,1,'2013-12-17 14:31:58.0')
,(10000,'White Label Legacy',3000.00,'month',9999,NULL,9999,1,1,1,1,1,0,0,1,'2013-11-05 17:45:13.0')
;

INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(1,'Web Analytics','Google Analytics','https://www.google.com/analytics/',3600,200,1,1,1,NULL,100,10000,0,'Google Account','OAUTH2',NULL,0,'Website','OAUTH2',NULL,0,'Metric','GA',NULL,0,'Segment','GA_SEGMENTS',NULL,0,'Filters','CUSTOM','<strong>Filter query format:</strong> ga:country==United States<br /><br /><a href=''https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn more.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Audience, Traffic, Content, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.google.com%2Fanalytics%2Ffeeds+email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles?start-index=1&max-results=2000&access_token={{ACCESS_TOKEN}}',NULL,'https://www.googleapis.com/analytics/v3/management/segments?max-results=1000&v=2&prettyprint=true&access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL)
,(2,'Social Media','Twitter','https://twitter.com/login/',1800,300,0,0,1,NULL,100,200,0,'Twitter Username','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tweets, Following, Followers, Listed, Mentions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(3,'Web Analytics','Alexa','http://www.alexa.com',86400,200,1,0,1,NULL,100,200,0,'Website','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Traffic Rank, Reach, Pageviews',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(4,'Advertising','Google AdSense','https://www.google.com/adsense',10800,220,1,0,1,NULL,100,200,0,'Google Account','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Earnings, Pageviews, Clicks, CTR, CPC, RPM',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadsense.readonly+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(5,'Social Media','Facebook Pages','http://www.facebook.com/login.php',21600,200,1,0,1,NULL,100,5000,0,'Facebook Account','OAUTH2',NULL,0,'Facebook Page','OAUTH2',NULL,0,'Facebook Metric','FACEBOOK_PAGES',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Likes, Clicks, Active Users, Pageviews, Talking',NULL,'https://www.facebook.com/v2.12/dialog/oauth?client_id=225327114206053&scope=read_insights,manage_pages&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://graph.facebook.com/v2.12/oauth/access_token?client_id=225327114206053&client_secret=e53df52a2b40dc3c9e411daecb58bf5d&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'https://graph.facebook.com/v2.12/me?access_token={{ACCESS_TOKEN}}','https://graph.facebook.com/v2.12/me/accounts?access_token={{ACCESS_TOKEN}}&limit=1000&fields=id,name,name_with_location_descriptor',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(6,'Other','Feedburner','http://feedburner.google.com/',21600,200,0,0,1,NULL,100,200,0,'FeedBurner URI','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Subscribers, Hits, Reach',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(7,'Custom','Private URL',NULL,10,200,1,0,1,NULL,100,50000,0,'Chart Type','CHART',NULL,0,'Private URL','URL','<a href=''http://www.cyfe.com/custom'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how you should format your data.|<a href=''/docs/custom'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how you should format your data.',0,'Refresh Rate','REFRESH',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Line, Area, Bar, Pie, List, Funnel, Table, Gauge',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(9,'Social Media','YouTube','https://accounts.google.com/ServiceLogin?service=youtube',3600,220,0,0,1,NULL,100,200,0,'YouTube Account','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Views, Users, Comments, Favorites, Likes',NULL,'https://accounts.google.com/o/oauth2/auth?scope=http%3A%2F%2Fgdata.youtube.com+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(11,'Sales & Finance','Salesforce','https://login.salesforce.com',21600,200,1,0,1,NULL,100,200,0,'Salesforce Account','OAUTH2',NULL,0,'Metric','SALESFORCE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Leads, Accounts, Contacts, Opportunities, Tasks',NULL,'https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=3MVG9QDx8IX8nP5QdQcTJDJYaZLErW6sQjxkRTrWuPzijvi8nJzrfUjsrbVU9PXu3FbZOKzkmXVFqCeVnsqoT&scope=web%20api%20refresh_token&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&state={{DASHBOARD_DWID}}','https://login.salesforce.com/services/oauth2/token?grant_type=authorization_code&client_id=3MVG9QDx8IX8nP5QdQcTJDJYaZLErW6sQjxkRTrWuPzijvi8nJzrfUjsrbVU9PXu3FbZOKzkmXVFqCeVnsqoT&client_secret=4559742001744949402&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&code={{OAUTH_AUTH_TOKEN}}','https://login.salesforce.com/services/oauth2/token?grant_type=refresh_token&client_id=3MVG9QDx8IX8nP5QdQcTJDJYaZLErW6sQjxkRTrWuPzijvi8nJzrfUjsrbVU9PXu3FbZOKzkmXVFqCeVnsqoT&client_secret=4559742001744949402&refresh_token={{REFRESH_TOKEN}}&format=json','[[id]]?oauth_token={{ACCESS_TOKEN}}&format=json',NULL,NULL,NULL,'instance_url',NULL,NULL,NULL,NULL)
,(20,'SEO','Google Webmasters','http://www.google.com/webmasters/',36000,200,1,0,1,NULL,100,200,0,'Google Account','OAUTH2',NULL,0,'Website','OAUTH2',NULL,0,'Metric','GWT',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Keywords, Sitemaps, Messages, Crawl Errors',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fwebmasters+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://www.googleapis.com/webmasters/v3/sites?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(25,'Custom','RSS Feed',NULL,900,250,1,0,1,NULL,100,200,0,'RSS Feed URL','URL',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'RSS Feed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(30,'Email','Constant Contact',NULL,3600,200,1,0,1,NULL,100,200,0,'Constant Contact Account','OAUTH2',NULL,0,'Constant Contact Campaign','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Emails Sent, Bounces, Unsubscribes, Forwards, Opens, Clicks',NULL,'https://oauth2.constantcontact.com/oauth2/oauth/siteowner/authorize?response_type=code&client_id=85b465e5-f1b5-4980-a374-04098695af10&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}','https://oauth2.constantcontact.com/oauth2/oauth/token?grant_type=authorization_code&client_id=85b465e5-f1b5-4980-a374-04098695af10&client_secret=3838df8e329b4e779258e493bf4186f9&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}',NULL,NULL,'https://api.constantcontact.com/ws/customers/{{NAME}}/campaigns?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(31,'Social Media','AddThis','https://www.addthis.com/login',21600,200,1,0,1,NULL,100,200,1728000,'AddThis Email','API',NULL,0,'AddThis Password','PASSWORD',NULL,0,'AddThis Publisher ID','API',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Shares, Clicks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(34,'Other','WordPress',NULL,3600,200,1,0,1,NULL,100,200,0,'API URL','URL','The API URL is located inside your WordPress admin section on the Cyfe Configuration page. You must install the <a href=''http://wordpress.org/extend/plugins/cyfe/'' target=''_blank'' style=''text-decoration:underline;''>Cyfe plugin</a> first.|The API URL is located inside your WordPress admin section on the Dashboard Configuration page. You must install the <a href=''/ext/wordpress/dashboard.zip'' target=''_blank'' style=''text-decoration:underline;''>Dashboard plugin</a> first.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Users, Posts, Comments',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(35,'Monitoring','Pingdom','https://pp.pingdom.com/index.php/login',10800,200,0,0,1,NULL,100,200,12960000,'Pingdom Email','API',NULL,0,'Pingdom Password','PASSWORD',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Uptime, Response Time',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(37,'Support','Zendesk','http://www.zendesk.com',3600,200,0,0,1,NULL,100,200,0,'Zendesk Username','API',NULL,0,'Zendesk Password','PASSWORD',NULL,0,'Zendesk Sitename','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tickets',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(38,'SEO','Moz','http://moz.com',432000,80,1,0,1,NULL,100,200,0,'URL','URL',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Backlinks, External Links, mozRank, Domain Authority, Page Authority',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(39,'Email','MailChimp','https://login.mailchimp.com/',3600,200,1,0,1,NULL,1,200,86400,'MailChimp Account','OAUTH2',NULL,0,'MailChimp Metric','OAUTH2',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'List Growth, Emails Sent, Bounces, Unsubscribes, Forwards, Opens, Clicks',NULL,'https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=296106821624&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}','https://login.mailchimp.com/oauth2/token?grant_type=authorization_code&client_id=296106821624&client_secret=ffdd67590b7a44f4b9e3c945a17635e1&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}',NULL,'[[api_endpoint]]/3.0/?method=getAccountDetails&apikey={{ACCESS_TOKEN}}&exclude[]=rewards-referrals','mailchimp',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(40,'Email','Gmail','https://mail.google.com',300,275,1,0,1,NULL,100,200,0,'Gmail Account','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Inbox, Unread Messages',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fgmail.readonly+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(42,'Email','SendGrid','http://sendgrid.com/login',3600,200,1,0,1,NULL,100,200,0,'SendGrid Username','API',NULL,0,'SendGrid Password','PASSWORD',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Email Requests, Spam, Bounces, Clicks, Opens',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(43,'Monitoring','AWS CloudWatch','https://console.aws.amazon.com',300,125,0,0,1,NULL,100,200,0,'Access Key','API',NULL,0,'Secret Key','API',NULL,0,'Metric','AWS_CW',NULL,0,'Region','AWS_REGIONS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'EC2, RDS, ELB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(47,'Other','Time',NULL,21600,100,1,0,1,NULL,100,200,0,'Time Zone','TIMEZONE',NULL,0,'','',NULL,0,'Display Format','TIMEFORMAT',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Digital Clock, Time Zones',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(48,'Other','Countdown',NULL,21600,100,1,0,1,NULL,100,200,0,'Countdown Date & Time','DATETIME',NULL,0,'Countdown End Message','HOORAY',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Countdown Timer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(49,'Sales & Finance','FreshBooks','http://www.freshbooks.com/',3600,200,1,0,1,NULL,100,200,0,'API URL','URL','The API URL is found on your Freshbooks <strong>My Account</strong> page.',0,'Authentication Token','API','The Authentication Token is found on your Freshbooks <strong>My Account</strong> page.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Invoices, Expenses',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(50,'Custom','CSV File',NULL,1576800000,200,1,0,1,NULL,100,500,0,'Chart Type','CHART',NULL,0,'CSV File','FILE','<a href=''http://www.cyfe.com/custom'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how you should format your data.|<a href=''/docs/custom'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how you should format your data.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Line, Area, Bar, Pie, List, Funnel, Table, Gauge',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(51,'Other','GoToWebinar','https://www2.gotomeeting.com/island/login.tmpl',3600,200,1,0,1,NULL,100,200,0,'GoToWebinar Account','OAUTH2',NULL,0,'Webinar','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Registrants',NULL,'https://api.getgo.com/oauth/v2/authorize?response_type=code&client_id=df6b95b9ba27ee5b951f48e8743ad6dc&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}','https://api.getgo.com/oauth/v2/token?grant_type=authorization_code&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}',NULL,NULL,'https://api.getgo.com/G2W/rest/organizers/{{NAME}}/webinars?oauth_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(52,'Custom','Image',NULL,1576800000,200,1,0,1,NULL,100,2000,0,'Image File','FILE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(53,'Custom','IFrame',NULL,900,200,1,0,1,NULL,100,1500,0,'IFrame URL','URL',NULL,0,'','',NULL,0,'Refresh Rate','REFRESH',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Web Pages, Slideshows',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(54,'Web Analytics','GoSquared','https://www.gosquared.com/cp/',30,200,1,0,1,NULL,1,200,0,'API Key','API','The API key is found inside your GoSquared account under settings.',0,'Site Token','API','The site token is found inside your GoSquared account under settings. It will be something like GSN-1234567-X.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Visitors Online, Returning Visitors, Active Pages',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(55,'Web Analytics','Chartbeat','http://chartbeat.com/chartbeat/signin/',15,200,1,0,1,NULL,1,200,0,'Domain Name','API','',0,'API Key','API','<a href=''http://chartbeat.com/apikeys'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to obtain your Chartbeat API key.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Visits, New Visits, Returning Visits',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(56,'Custom','Google Spreadsheets',NULL,300,200,1,0,1,NULL,100,1000,0,'Google Account','OAUTH2',NULL,0,'Spreadsheet','OAUTH2','<p>Learn how you should prepare your sheet:<ul style=''margin: 1px 0 0 10px;padding: 1px 0 0 10px;list-style:square;''><li style=''line-height: 2em;''><a href=''https://www.cyfe.com/custom'' target=''_blank'' style=''text-decoration:underline;''>Data formatting</a></li><li style=''line-height: 2em;''><a href=''https://help.cyfe.com/help/google-spreadsheets-widget-integration'' target=''_blank'' style=''text-decoration:underline;''>Sheet sharing</a></li></ul></p>|',0,'Chart Type','CHART',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Line, Area, Bar, Pie, List, Funnel, Table, Gauge',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fspreadsheets.readonly+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.readonly+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://spreadsheets.google.com/feeds/spreadsheets/private/full?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(57,'Social Media','Twitter Search','https://twitter.com/search/',300,300,1,0,1,NULL,100,200,0,'Twitter Account','OAUTH',NULL,0,'Twitter Keyword','API','You can use any of the Twitter <a href=''https://i.imgur.com/SeB6V3s.png'' target=''_blank'' style=''text-decoration:underline;''>search operators</a>.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tweets, Mentions, Hashtags','https://api.twitter.com/oauth/request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=BhM8j5la8EWMQ2n5GaGAQ&oauth_secret_key=SFIdGHR2eafxeAaPAvXlZYkhh0sVhReiCURrG3oYUdI&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://api.twitter.com/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.twitter.com/oauth/access_token?oauth_consumer_key=BhM8j5la8EWMQ2n5GaGAQ&oauth_secret_key=SFIdGHR2eafxeAaPAvXlZYkhh0sVhReiCURrG3oYUdI&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(58,'Social Media','Pinterest','http://pinterest.com/login/',3600,80,0,0,1,NULL,100,200,0,'Pinterest Username','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Boards, Pins, Likes, Followers, Following',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(59,'Social Media','LinkedIn','https://www.linkedin.com/uas/login',3600,300,0,0,1,NULL,100,200,0,'LinkedIn Account','OAUTH',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Connections, Network Updates','https://api.linkedin.com/uas/oauth/requestToken?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://www.linkedin.com/uas/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.linkedin.com/uas/oauth/accessToken?oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'http://api.linkedin.com/v1/people/~?oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(60,'Advertising','Google AdWords','https://adwords.google.com',900,220,0,0,1,NULL,100,200,0,'Google Account','OAUTH2',NULL,0,'AdWords Campaign','OAUTH2',NULL,0,'Metric','ADWORDS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fadwords.google.com%2Fapi%2Fadwords+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={{ACCESS_TOKEN}}','google_adwords',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(61,'Monitoring','Google Alerts','http://www.google.com/alerts',1080,250,1,0,1,NULL,100,200,0,'Feed URL','API','Enter the RSS feed URL provided by Google Alerts (e.g. http://www.google.com/alerts/feeds/XXXXX)',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'RSS Feed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(62,'Monitoring','Google Trends','http://www.google.com/trends/',604800,200,1,0,1,NULL,100,300,0,'Search Term','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Search Trends',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(64,'Social Media','Bitly','https://bitly.com',1800,200,1,0,1,NULL,100,200,0,'Bitly Account','OAUTH2',NULL,0,'Bitly Link','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Shares, Clicks',NULL,'https://bitly.com/oauth/authorize?client_id=d32650d87e5f37f6c801b48db89af75fce8f4df4&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}','https://api-ssl.bitly.com/oauth/access_token?code={{OAUTH_AUTH_TOKEN}}&client_id=d32650d87e5f37f6c801b48db89af75fce8f4df4&client_secret=492f839e36c5b9fdd552e45f5a40ca60bb3d2aac&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}',NULL,NULL,'https://api-ssl.bitly.com/v3/user/link_history?access_token={{ACCESS_TOKEN}}&limit=100',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(65,'Web Analytics','Quantcast','http://www.quantcast.com',1728000,200,1,0,1,NULL,100,200,0,'Website','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'People, Visits',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(66,'Web Analytics','Compete','http://www.compete.com',1728000,200,0,0,1,NULL,100,200,0,'Website','API',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Visitors',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(67,'Sales & Finance','PayPal','https://www.paypal.com',3600,200,0,0,0,NULL,100,200,0,'PayPal Account','PAYPAL',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Balance, Activity','https://svcs.paypal.com/Permissions/RequestPermissions?requestEnvelope.errorLanguage=en_US&scope(0)=ACCESS_BASIC_PERSONAL_DATA&scope(1)=ACCOUNT_BALANCE&scope(2)=TRANSACTION_SEARCH&callback={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}','https://www.paypal.com/cgi-bin/webscr?cmd=_grant-permission&request_token={{OAUTH_AUTH_TOKEN}}','https://svcs.paypal.com/Permissions/GetAccessToken?requestEnvelope.errorLanguage=en_US&token={{REQUEST_TOKEN}}&verifier={{VERIFICATION_CODE}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(70,'Social Media','YouTube','https://accounts.google.com/ServiceLogin?service=youtube',3600,220,1,1,1,NULL,100,500,0,'YouTube Account','OAUTH2',NULL,0,'YouTube Video','OAUTH2',NULL,0,'YouTube Metric','YOUTUBE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Views, Demographics, Audience, Engagement',NULL,'https://accounts.google.com/o/oauth2/auth?scope=http%3A%2F%2Fgdata.youtube.com+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fyt-analytics.readonly+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(73,'SEO','SERPs',NULL,86400,200,1,1,1,NULL,1,100,864000,'Keyword','API',NULL,1,'Domain Name','API',NULL,1,'Search Engine','SERPS_ENGINES',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Keyword Rankings, Search Engine Positions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(74,'Social Media','Twitter','https://twitter.com/login/',3600,200,1,1,1,NULL,2,500,86400,'Twitter Account','OAUTH',NULL,0,'Twitter Username','API','Enter the Twitter handle that you would like to monitor. This could be your own handle or your competitor''s.',1,'Twitter Metric','TWITTER',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tweets, Followers, Listed, Mentions, Influencers','https://api.twitter.com/oauth/request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=BhM8j5la8EWMQ2n5GaGAQ&oauth_secret_key=SFIdGHR2eafxeAaPAvXlZYkhh0sVhReiCURrG3oYUdI&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://api.twitter.com/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.twitter.com/oauth/access_token?oauth_consumer_key=BhM8j5la8EWMQ2n5GaGAQ&oauth_secret_key=SFIdGHR2eafxeAaPAvXlZYkhh0sVhReiCURrG3oYUdI&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(76,'Social Media','Pinterest','http://pinterest.com/login/',86400,200,1,0,1,NULL,1,200,86400,'Pinterest Username','API',NULL,1,'Metric','PINTEREST',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Boards, Pins, Likes, Followers, Following',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(77,'Custom','Push API',NULL,300,200,1,0,1,NULL,100,200,0,'Chart Type','CHART',NULL,0,'API Endpoint','PUSH_API','<a href=''http://www.cyfe.com/api'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how the Push API works.|<a href=''/docs/api'' target=''_blank'' style=''text-decoration:underline;''>Click here</a> to learn about how the Push API works.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Line, Area, Column, Bar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(78,'Advertising','Google AdWords (MCC)','https://adwords.google.com',900,220,0,0,1,NULL,100,1000,0,'Google Account','OAUTH2',NULL,0,'AdWords Client','OAUTH2',NULL,0,'Metric','ADWORDS',NULL,0,'AdWords Campaign','ADWORDS_MCC_CAMPAIGNS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fadwords.google.com%2Fapi%2Fadwords+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={{ACCESS_TOKEN}}','google_adwords_mcc',NULL,'google_adwords_mcc_campaigns',NULL,NULL,NULL,NULL,NULL)
,(79,'Email','AWeber','https://www.aweber.com/login.htm',3600,200,1,0,0,NULL,100,200,0,'AWeber Account','OAUTH',NULL,0,'AWeber List','OAUTH2',NULL,0,'','',NULL,0,'AWeber Campaign','AWEBER_CAMPAIGNS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Emails Sent, Spam, Unsubscribes, Opens, Clicks','https://auth.aweber.com/1.0/oauth/request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=AkBFl4NoA8BhzTjtJI2rT3Zh&oauth_secret_key=dM73m9w9678f6Ee8Rr20qqaEFp037moMKZbQRIf4&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://auth.aweber.com/1.0/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://auth.aweber.com/1.0/oauth/access_token?oauth_consumer_key=AkBFl4NoA8BhzTjtJI2rT3Zh&oauth_secret_key=dM73m9w9678f6Ee8Rr20qqaEFp037moMKZbQRIf4&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'https://api.aweber.com/1.0/accounts?oauth_consumer_key=AkBFl4NoA8BhzTjtJI2rT3Zh&oauth_secret_key=dM73m9w9678f6Ee8Rr20qqaEFp037moMKZbQRIf4&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0','aweber',NULL,'aweber',NULL,NULL,NULL,NULL,NULL)
,(80,'Email','Campaign Monitor','https://login.createsend.com/l',3600,200,1,0,1,NULL,1,200,86400,'Campaign Monitor Account','OAUTH2',NULL,0,'Campaign Monitor Client','OAUTH2',NULL,1,'','',NULL,0,'Campaign Monitor Metric','CAMPAIGN_MONITOR',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'List Growth, Emails Sent, Bounces, Spam, Unsubscribes, Forwards, Opens, Clicks',NULL,'https://api.createsend.com/oauth?type=web_server&client_id=98891&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&scope=ManageLists,AdministerAccount,ViewReports&state={{DASHBOARD_DWID}}','https://api.createsend.com/oauth/token?grant_type=authorization_code&client_id=98891&client_secret=KMKYZ3DkBeK3TO3hO353qIsQic39PZA31r3BDMX33mZ0373K3iQz3I3nRIhmNOyBqFU733332Q538P3s&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://api.createsend.com/oauth/token?grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://api.createsend.com/api/v3/primarycontact.json?access_token={{ACCESS_TOKEN}}','campaign_monitor',NULL,'campaign_monitor',NULL,NULL,NULL,NULL,NULL)
,(81,'Social Media','Instagram','https://instagram.com/accounts/login/',86400,200,0,0,1,NULL,1,200,86400,'Instagram Account','OAUTH2',NULL,0,'Instagram Username','API','Enter the Instagram username that you would like to monitor. This could be your own username or your competitor''s.',1,'Metric','INSTAGRAM',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Photos, Followers, Following',NULL,'https://api.instagram.com/oauth/authorize/?client_id=bc39b1295ae44ccd91c9134455c0b94f&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&state={{DASHBOARD_DWID}}&scope=basic+public_content','https://api.instagram.com/oauth/access_token?grant_type=authorization_code&client_id=bc39b1295ae44ccd91c9134455c0b94f&client_secret=bb49822c1c604f2a85e450ca85c68bbf&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(83,'Social Media','LinkedIn Company','https://www.linkedin.com/uas/login',86400,200,0,0,1,NULL,1,200,86400,'LinkedIn Account','OAUTH',NULL,0,'LinkedIn Company URL','API','Enter the LinkedIn URL of your or your competitor''s company. For example:<br />http://www.linkedin.com/company/google',1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Followers','https://api.linkedin.com/uas/oauth/requestToken?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://www.linkedin.com/uas/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.linkedin.com/uas/oauth/accessToken?oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'http://api.linkedin.com/v1/people/~?oauth_consumer_key=t66e8y00mqvl&oauth_secret_key=bmjNDKSXSq7mdOpe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(84,'Advertising','Google Doubleclick for Publishers','https://www.google.com/dfp',21600,200,0,0,1,NULL,100,200,0,'Google Account','OAUTH2',NULL,0,'DFP Network','OAUTH2',NULL,0,'','',NULL,0,'DFP Line Item','GOOGLE_DFP',NULL,0,'Metric','GDFP',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Impressions, Clicks, CTR, Revenue',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdfp%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','google_dfp',NULL,'google_dfp',NULL,NULL,NULL,NULL,NULL)
,(85,'Other','iTunes Connect','https://itunesconnect.apple.com',21600,200,0,0,1,NULL,1,200,86400,'iTunes Connect ID','API',NULL,0,'iTunes Connect Access Token','PASSWORD','From the ITC homepage, click Sales and Trends. In the upper-left corner, click the pop-up menu and choose Reports. In the upper-right corner, click on the tooltip next to About Reports. You will either have to click generate access token or view the one given.',0,'iTunes Connect Vendor ID','API','The vendor ID is found inside your iTunes Connect account (e.g. 8#######).',1,'iTunes Connect Metric','ITUNES_CONNECT','',1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'App Downloads, Updates, Revenue, Purchases',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(87,'Custom','Text',NULL,31536000,200,1,1,1,NULL,100,1000,0,'Widget Text','TEXT',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Rich Text, HTML',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(88,'Web Analytics','Mixpanel','https://mixpanel.com/login/',600,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Events, Properties, People',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(89,'Advertising','Marchex','https://www.voicestar.com/login',3600,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Call Log, Calls, Campaigns',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(90,'Advertising','ReachLocal [NSD]','http://www.reachlocal.com/',3600,200,1,0,1,34876,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Activity, Metrics, Call Log',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(91,'Sales & Finance','Infusionsoft','https://signin.infusionsoft.com/login',3600,200,1,0,1,NULL,1,200,86400,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Companies, Contacts, Opportunities, Sales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(92,'Web Analytics','Google Analytics [Legacy]','https://www.google.com/analytics/',3600,200,1,0,1,29751,100,15000,0,'Google Account','OAUTH2',NULL,0,'Website','OAUTH2',NULL,0,'Metric','GA',NULL,0,'Segment','GA_SEGMENTS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Audience, Traffic, Content, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.google.com%2Fanalytics%2Ffeeds+email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles?start-index=1&max-results=1000&access_token={{ACCESS_TOKEN}}',NULL,'https://www.googleapis.com/analytics/v3/management/segments?max-results=500&v=2&prettyprint=true&access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL)
,(93,'Web Analytics','Google Analytics (Real Time)','https://www.google.com/analytics/',60,200,1,0,1,NULL,1,1000,0,'Google Account','OAUTH2',NULL,0,'Website','OAUTH2',NULL,0,'Metric','GART',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Audience, Traffic, Content',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.google.com%2Fanalytics%2Ffeeds+email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles?start-index=1&max-results=1000&access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(95,'Other','Weather',NULL,3600,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Local Weather',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(96,'Advertising','Bing Ads','https://bingads.microsoft.com',3600,220,1,0,1,NULL,100,200,0,'Microsoft Account','OAUTH2',NULL,0,'Bing Ads Customer','OAUTH2',NULL,0,'Bing Ads Account','OAUTH2',NULL,0,'Bing Ads Campaign','BINGADS_CAMPAIGNS',NULL,0,'Metric','BINGADS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://login.live.com/oauth20_authorize.srf?client_id=0000000040115534&scope=bingads.manage&response_type=code&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://login.live.com/oauth20_token.srf?client_id=0000000040115534&client_secret=AU4NWLJiR707jUKF4treFR698YkNbrBS&code={{OAUTH_AUTH_TOKEN}}&grant_type=authorization_code&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://login.live.com/oauth20_token.srf?client_id=0000000040115534&client_secret=AU4NWLJiR707jUKF4treFR698YkNbrBS &grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'bingads','bingads','bingads',NULL,NULL,NULL,NULL,NULL)
,(97,'Other','Google Calendar','https://www.google.com/calendar',900,200,1,0,1,NULL,100,200,0,'Google Account','OAUTH2',NULL,0,'Calendar','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Upcoming Events',NULL,'https://accounts.google.com/o/oauth2/auth?scope=email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar.readonly&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}','https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(98,'Email','iContact','https://www.icontact.com/login',3600,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'List Growth, Emails Sent, Bounces, Unsubscribes, Forwards, Opens, Clicks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(99,'Other','Basecamp','https://basecamp.com',3600,200,1,0,1,NULL,100,600,0,'Basecamp Account','OAUTH2',NULL,0,'Basecamp Company','OAUTH2',NULL,0,'','',NULL,0,'Basecamp Project','BASECAMP_PROJECTS',NULL,0,'Basecamp Metric','BASECAMP',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Discussions, Events, People, To-Dos, Updates',NULL,'https://launchpad.37signals.com/authorization/new?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://launchpad.37signals.com/authorization/token?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&code={{OAUTH_AUTH_TOKEN}}','https://launchpad.37signals.com/authorization/token?type=refresh&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&refresh_token={{REFRESH_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}',NULL,'basecamp',NULL,NULL,NULL,NULL,NULL)
,(101,'Social Media','SlideShare','http://www.slideshare.net',86400,200,1,0,1,NULL,1,200,86400,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Views, Favorites, Comments, Downloads',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(102,'Other','Basecamp Classic','https://basecamp.com',3600,200,1,0,1,NULL,100,600,0,'Basecamp Classic Account','OAUTH2',NULL,0,'Basecamp Classic Company','OAUTH2',NULL,0,'','',NULL,0,'Basecamp Classic Project','BASECAMP_CLASSIC_PROJECTS',NULL,0,'Basecamp Classic Metric','BASECAMP_CLASSIC',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Discussions, Events, People, Time, To-Dos',NULL,'https://launchpad.37signals.com/authorization/new?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://launchpad.37signals.com/authorization/token?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&code={{OAUTH_AUTH_TOKEN}}','https://launchpad.37signals.com/authorization/token?type=refresh&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&refresh_token={{REFRESH_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}',NULL,'basecamp_classic',NULL,NULL,NULL,NULL,NULL)
,(103,'Social Media','Vimeo','https://vimeo.com',3600,200,1,0,1,NULL,100,600,0,'Vimeo Account','OAUTH2',NULL,0,'Vimeo Channel','OAUTH2',NULL,0,'','',NULL,0,'Vimeo Video','VIMEO_VIDEOS',NULL,0,'Vimeo Metric','VIMEO',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'My Feed, Top Videos',NULL,'https://api.vimeo.com/oauth/authorize?response_type=code&client_id=579826bdb4ec52c40918fad87ae617d3ff22c3e4&scope=private&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://api.vimeo.com/oauth/access_token?grant_type=authorization_code&code={{OAUTH_AUTH_TOKEN}}&client_id=579826bdb4ec52c40918fad87ae617d3ff22c3e4&client_secret=0d9fd28938f002035b7131fdda289d3c53e404fe&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,NULL,'vimeo',NULL,'vimeo',NULL,NULL,NULL,NULL,NULL)
,(104,'Monitoring','Pingdom','https://my.pingdom.com',300,200,1,0,1,NULL,100,200,12960000,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Uptime, Response Time, Logs',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(105,'Support','Desk','http://www.desk.com',3600,200,0,0,0,NULL,100,200,0,'Desk Account','OAUTH',NULL,0,'Desk Metric','DESK',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cases, Agents, Groups, Labels, Macros','https://{{OAUTH_CUSTOM_URL}}/oauth/request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=gOY9Yp03VJxzg4w26GvH&oauth_secret_key=maAKXFZLMYZXFZkWLUrI3I3XHmncXeu3sH8hM9vR&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://{{OAUTH_CUSTOM_URL}}/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://{{OAUTH_CUSTOM_URL}}/oauth/access_token?oauth_consumer_key=gOY9Yp03VJxzg4w26GvH&oauth_secret_key=maAKXFZLMYZXFZkWLUrI3I3XHmncXeu3sH8hM9vR&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'https://{{OAUTH_CUSTOM_URL}}/api/v2/users/current?oauth_consumer_key=gOY9Yp03VJxzg4w26GvH&oauth_secret_key=maAKXFZLMYZXFZkWLUrI3I3XHmncXeu3sH8hM9vR&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0',NULL,NULL,NULL,NULL,'Desk URL','For example: yoursite.desk.com',NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(106,'Email','GetResponse','https://app.getresponse.com/login.html',3600,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'List Growth, Emails Sent, Bounces, Unsubscribes, Forwards, Opens, Clicks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(107,'Social Media','Flickr','https://www.flickr.com/signin',3600,200,1,0,1,NULL,100,200,0,'Flickr Account','OAUTH',NULL,0,'Flickr Metric','FLICKR',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Top Photos, Slideshows','https://www.flickr.com/services/oauth/request_token?oauth_nonce={{NONCE}}&oauth_timestamp={{TIME}}&oauth_consumer_key=ff45956a6dac98cdead46e622008dfd0&oauth_secret_key=530722b990575a04&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_version=1.0&oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}','https://www.flickr.com/services/oauth/authorize?perms=read&oauth_token={{OAUTH_AUTH_TOKEN}}','https://www.flickr.com/services/oauth/access_token?oauth_nonce={{NONCE}}&oauth_timestamp={{TIME}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_consumer_key=ff45956a6dac98cdead46e622008dfd0&oauth_secret_key=530722b990575a04&oauth_signature_method=HMAC-SHA1&oauth_version=1.0&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_signature={{OAUTH_SIGNATURE}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(108,'Monitoring','AWS CloudWatch','https://console.aws.amazon.com',300,200,1,0,1,NULL,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'EC2, RDS, ELB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(109,'Web Analytics','Unbounce','https://app.unbounce.com/login',3600,200,1,0,1,NULL,100,200,0,'Unbounce Account','OAUTH2',NULL,0,'Unbounce Client','OAUTH2',NULL,0,'','',NULL,0,'Unbounce Page','UNBOUNCE_PAGES',NULL,0,'Unbounce Metric','UNBOUNCE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Conversion Funnel, Leads, Top Pages',NULL,'https://api.unbounce.com/oauth/authorize?response_type=code&client_id=d74c3fb869351ddb2b726cbd4edd3872b5d11aa3e407219c7e978d042b850fee&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://api.unbounce.com/oauth/token?grant_type=authorization_code&code={{OAUTH_AUTH_TOKEN}}&client_id=d74c3fb869351ddb2b726cbd4edd3872b5d11aa3e407219c7e978d042b850fee&client_secret=f7603f11b13507741b01e640dee4a96ca9d84a63448d903c201b4b01287205e3&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'https://api.unbounce.com/accounts','unbounce',NULL,'unbounce',NULL,NULL,NULL,NULL,NULL)
,(110,'Support','Zendesk','http://www.zendesk.com/login',3600,200,1,0,1,NULL,1,200,86400,'Zendesk Account','OAUTH2',NULL,0,'Zendesk Metric','ZENDESK',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tickets, Agents, Groups, Tags, Satisfaction',NULL,'https://{{OAUTH_CUSTOM_URL}}/oauth/authorizations/new?scope=read&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=dashboard','https://{{OAUTH_CUSTOM_URL}}/oauth/tokens?code={{OAUTH_AUTH_TOKEN}}&client_id=dashboard&client_secret=1a0c3f822941ab20668ea5b15621d1a8de0ee12d05b724e1864f8dc5ac296a73&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&scope=read&grant_type=authorization_code',NULL,'https://{{OAUTH_CUSTOM_URL}}/api/v2/users/me.json',NULL,NULL,NULL,NULL,'Zendesk URL','For example: yoursite.zendesk.com',NULL,NULL)
,(111,'Sales & Finance','Shopify','https://www.shopify.com/login',900,200,1,0,0,NULL,1,200,86400,'Shopify Account','OAUTH2',NULL,0,'Shopify Metric','SHOPIFY',NULL,1,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Orders, Sales',NULL,'https://{{OAUTH_CUSTOM_URL}}/admin/oauth/authorize?client_id=76f8d620dd5e6b585478e7db0b99212d&state={{DASHBOARD_DWID}}&scope=read_content,read_products,read_customers,read_orders,read_fulfillments,read_shipping&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://{{OAUTH_CUSTOM_URL}}/admin/oauth/access_token?code={{OAUTH_AUTH_TOKEN}}&client_id=76f8d620dd5e6b585478e7db0b99212d&client_secret=0737186659939fab86fb1420718aabf9',NULL,'https://{{OAUTH_CUSTOM_URL}}/admin/shop.json',NULL,NULL,NULL,NULL,'Shopify URL','For example: shopname.myshopify.com',NULL,NULL)
,(112,'Sales & Finance','QuickBooks','https://qbo.intuit.com',3600,200,1,0,0,NULL,100,2000,0,'QuickBooks Account','OAUTH',NULL,0,'QuickBooks Metric','QUICKBOOKS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Income, Expenses, Sales, Customers, Vendors','https://oauth.intuit.com/oauth/v1/get_request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=qyprdkybuKrfFCrN1ouTbvQfBSvuIZ&oauth_secret_key=dVXV6VAlmENb5gHLcRQUI2dmgkNHMerUEbX8jS4d&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://appcenter.intuit.com/Connect/Begin?oauth_token={{OAUTH_AUTH_TOKEN}}','https://oauth.intuit.com/oauth/v1/get_access_token?oauth_consumer_key=qyprdkybuKrfFCrN1ouTbvQfBSvuIZ&oauth_secret_key=dVXV6VAlmENb5gHLcRQUI2dmgkNHMerUEbX8jS4d&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0','https://appcenter.intuit.com/api/v1/connection/reconnect?oauth_consumer_key=qyprdkybuKrfFCrN1ouTbvQfBSvuIZ&oauth_secret_key=dVXV6VAlmENb5gHLcRQUI2dmgkNHMerUEbX8jS4d&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0','https://appcenter.intuit.com/api/v1/user/current?oauth_consumer_key=qyprdkybuKrfFCrN1ouTbvQfBSvuIZ&oauth_secret_key=dVXV6VAlmENb5gHLcRQUI2dmgkNHMerUEbX8jS4d&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(113,'Sales & Finance','Xero','https://login.xero.com',3600,200,1,0,0,NULL,100,300,0,'Xero Account','OAUTH',NULL,0,'Xero Metric','XERO',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Income, Expenses, Sales, Customers, Suppliers','https://api.xero.com/oauth/RequestToken?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=52WBIZOE66AQKKOY6MY1JNAWCWUREO&oauth_secret_key=EKXW1KEVTIJ82YH4ZWKZD9Z0DXK6IW&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=RSA-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://api.xero.com/oauth/Authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.xero.com/oauth/AccessToken?oauth_consumer_key=52WBIZOE66AQKKOY6MY1JNAWCWUREO&oauth_secret_key=EKXW1KEVTIJ82YH4ZWKZD9Z0DXK6IW&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=RSA-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0','https://api.xero.com/oauth/AccessToken?oauth_consumer_key=52WBIZOE66AQKKOY6MY1JNAWCWUREO&oauth_secret_key=EKXW1KEVTIJ82YH4ZWKZD9Z0DXK6IW&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=RSA-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_session_handle={{OAUTH_SESSION_HANDLE}}&oauth_version=1.0','https://api.xero.com/api.xro/2.0/Organisation?oauth_consumer_key=52WBIZOE66AQKKOY6MY1JNAWCWUREO&oauth_secret_key=EKXW1KEVTIJ82YH4ZWKZD9Z0DXK6IW&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=RSA-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_version=1.0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(114,'Advertising','Facebook Ads','https://www.facebook.com/ads',3600,200,1,1,1,NULL,100,5000,0,'Facebook Account','OAUTH2',NULL,0,'Facebook Ad Account','OAUTH2',NULL,0,'Facebook Ad Campaign','FACEBOOK_AD_CAMPAIGNS',NULL,0,'Facebook Action Type','FACEBOOK_ACTION_TYPES',NULL,0,'Facebook Metric','FACEBOOK_ADS',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Actions',NULL,'https://www.facebook.com/v5.0/dialog/oauth?client_id=225327114206053&scope=ads_read&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://graph.facebook.com/v5.0/oauth/access_token?client_id=225327114206053&client_secret=e53df52a2b40dc3c9e411daecb58bf5d&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'https://graph.facebook.com/v5.0/me?access_token={{ACCESS_TOKEN}}','https://graph.facebook.com/v5.0/me/adaccounts?fields=name&limit=1000&access_token={{ACCESS_TOKEN}}','facebook_ad_campaigns','facebook_action_types',NULL,NULL,NULL,NULL,NULL)
,(115,'Sales & Finance','Eventbrite','https://www.eventbrite.com/login',3600,200,1,0,0,NULL,1,200,86400,'Eventbrite Account','OAUTH2',NULL,0,'Eventbrite Ticket','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'Eventbrite Metric','EVENTBRITE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tickets, Sales, Orders',NULL,'https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=PZAPAX3V7F3HBUKTM5&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://www.eventbrite.com/oauth/token?code={{OAUTH_AUTH_TOKEN}}&client_secret=UKTJ244GRFSFW6VFNEJKCUCIUPBFYLGYOIESRL6YJF4IPGLNUI&client_id=PZAPAX3V7F3HBUKTM5&grant_type=authorization_code&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'https://www.eventbriteapi.com/v3/users/me/?token={{ACCESS_TOKEN}}','eventbrite',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(116,'Other','Highrise','https://highrisehq.com',3600,200,1,0,1,NULL,100,600,0,'Highrise Account','OAUTH2',NULL,0,'Highrise Company','OAUTH2',NULL,0,'','',NULL,0,'','',NULL,0,'Highrise Metric','HIGHRISE',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cases, Contacts, Deals, Tasks',NULL,'https://launchpad.37signals.com/authorization/new?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://launchpad.37signals.com/authorization/token?type=web_server&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&code={{OAUTH_AUTH_TOKEN}}','https://launchpad.37signals.com/authorization/token?type=refresh&client_id=f9cdf179cc8e90ffd94b8db9df30a0a4cb908c65&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&client_secret=a9e6a217540ea8bac75df094c0c2c63ece7bca1c&refresh_token={{REFRESH_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}','https://launchpad.37signals.com/authorization.json?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(117,'Sales & Finance','Recurly','https://app.recurly.com/login',3600,200,1,0,1,NULL,1,200,172800,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Accounts, Subscribers, Transactions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(118,'Other','Apex Chat [NSD]','https://www.apexchat.com/Pages/Portal.aspx',3600,200,1,0,1,34876,100,200,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Lead Log, Leads',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(119,'Sales & Finance','Stripe','https://dashboard.stripe.com/login',3600,200,1,0,1,NULL,1,200,172800,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Customers, Subscriptions, Charges',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(122,'Custom','SQL',NULL,60,200,1,1,1,NULL,100,1000,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'','CUSTOM',NULL,0,'Line, Area, Bar, Pie, List, Funnel, Table, Gauge',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(123,'Social Media','Twitter Ads','https://ads.twitter.com/',1500,200,1,0,1,NULL,100,200,0,'Twitter Account','OAUTH','',0,'Twitter Ad Account','OAUTH2',NULL,0,'Twitter Ad Campaign','OAUTH2',NULL,0,'Twitter Metric','BINGADS_CAMPAIGNS',NULL,0,'Twitter Segment','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impression, Engagements, Follows','https://api.twitter.com/oauth/request_token?oauth_callback=https%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate%3D{{DASHBOARD_DWID}}&oauth_consumer_key=hsDq2xcC4xEjPosWtQG7svVT0&oauth_secret_key=A0wb2hQmqqSYLgG5dN0qZ8oqDpEHsSIJLnabxbbzgAbe7wWEvp&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0','https://api.twitter.com/oauth/authorize?oauth_token={{OAUTH_AUTH_TOKEN}}','https://api.twitter.com/oauth/access_token?oauth_consumer_key=hsDq2xcC4xEjPosWtQG7svVT0&oauth_secret_key=A0wb2hQmqqSYLgG5dN0qZ8oqDpEHsSIJLnabxbbzgAbe7wWEvp&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'','TwitterAds','TwitterAds','TwitterAds',NULL,NULL,NULL,NULL,NULL)
,(124,'Sales & Finance','MYOB Essentials','https://essentials.myob.com.au/',1200,200,1,0,1,NULL,100,200,0,'MYOB Account','OAUTH2',NULL,0,'Essentials Business Region','CUSTOM',NULL,0,'Business','OAUTH2',NULL,0,'Metric','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Balances, Customers, Suppliers, Orders','','https://secure.myob.com/oauth2/account/authorize?client_id=9wm226b67bn8v8yj2uqwjb3x&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&scope=CompanyFile%20la.global&state={{DASHBOARD_DWID}}','https://secure.myob.com/oauth2/v1/authorize','https://secure.myob.com/oauth2/v1/authorize?client_id=9wm226b67bn8v8yj2uqwjb3x&client_secret=uFsUCCj9pwV6EwPsxBwn6bGw&refresh_token={{REFRESH_TOKEN}}&grant_type=refresh_token',NULL,NULL,'MYOB',NULL,NULL,NULL,NULL,NULL,NULL)
,(125,'Sales & Finance','MYOB AccountRight','http://myob.com.au/',3600,200,1,0,1,NULL,100,200,0,'MYOB Account','OAUTH2',NULL,0,'Username','CUSTOM',NULL,0,'Password','CUSTOM',NULL,0,'File','CUSTOM',NULL,0,'Metric','CUSTOM',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Balances, Customers, Suppliers, Orders',NULL,'https://secure.myob.com/oauth2/account/authorize?client_id=9wm226b67bn8v8yj2uqwjb3x&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&scope=CompanyFile%20la.global&state={{DASHBOARD_DWID}}','https://secure.myob.com/oauth2/v1/authorize','https://secure.myob.com/oauth2/v1/authorize?client_id=9wm226b67bn8v8yj2uqwjb3x&client_secret=uFsUCCj9pwV6EwPsxBwn6bGw&refresh_token={{REFRESH_TOKEN}}&grant_type=refresh_token',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(127,'Sales & Finance','Paywhirl','https://app.paywhirl.com/',3600,200,1,0,0,NULL,100,200,0,'URL','URL','Enable the Cyfe integration in your Paywhirl account, click the Settings button, and copy/paste your desired metric URL here.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Customers, Invoices, Subscriptions, Sales','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(128,'Sales & Finance','Tubular.io    ','http://www.tubular.io',3600,200,1,0,0,NULL,100,100,0,'URL','URL','Once you have created your desired Report, right click on the Export CSV Report button, click Copy Link Address, and paste that URL here.',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,' Leads, Deals, Proposals, Sales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(130,'Email','ActiveCampaign','http://www.activecampaign.com/',3600,200,1,0,1,NULL,100,100,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Emails Sent, Bounces, Unsubscribes, Forwards, Opens, Clicks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(131,'Social Media','Instagram','https://www.instagram.com/accounts/login/',86400,200,0,0,1,NULL,100,200,86400,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Photos, Followers, Following, Likes, Comments',NULL,'https://api.instagram.com/oauth/authorize/?client_id=bc39b1295ae44ccd91c9134455c0b94f&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&state={{DASHBOARD_DWID}}&scope=basic+public_content','https://api.instagram.com/oauth/access_token?grant_type=authorization_code&client_id=bc39b1295ae44ccd91c9134455c0b94f&client_secret=bb49822c1c604f2a85e450ca85c68bbf&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(132,'Social Media','LinkedIn Company','https://www.linkedin.com/uas/login',14400,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Followers, Impressions, Engagements','','https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=t66e8y00mqvl&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&state={{DASHBOARD_DWID}}&scope=rw_organization_admin%20r_basicprofile%20r_member_social%20r_ads%20r_ads_reporting%20r_organization_social%20rw_ads','https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&state={{DASHBOARD_DWID}}&client_id=t66e8y00mqvl&client_secret=bmjNDKSXSq7mdOpe',NULL,'https://api.linkedin.com/v2/me?oauth2_access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(133,'Social Media','Pinterest (beta)','http://pinterest.com/login/',3600,200,0,0,1,NULL,100,200,86400,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,' Engagements, Likes, Comments, Repins',NULL,'https://api.pinterest.com/oauth/?response_type=code&client_id=5050516800809254427&state={{DASHBOARD_DWID}}&scope=read_public&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://api.pinterest.com/v1/oauth/token?grant_type=authorization_code&client_id=5050516800809254427&client_secret=df2c6004e4cf97f8ebd219c9bdf8246159cf0b7c8ebcf5ab74a854c28b8fdb0b&code={{OAUTH_AUTH_TOKEN}}',NULL,'https://api.pinterest.com/v1/me/?access_token={{ACCESS_TOKEN}}&fields=first_name%2Cid%2Clast_name%2Curl',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(134,'Email','MyEmma','http://myemma.com/',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Forwards, Bounces, Link Clicks, Opens, Unsubscriptions',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(136,'Social Media','Publisher',NULL,86400,400,1,1,1,NULL,1,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Schedule Social Media Posts',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(137,'Custom','Mashup',NULL,21600,200,1,1,1,NULL,1,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Combine Multiple Widget Metrics',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(138,'Other','iTunes Connect','https://itunesconnect.apple.com',21600,200,1,0,1,NULL,1,200,86400,'','CUSTOM','',0,'','CUSTOM','',0,'','CUSTOM','',0,'','','',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'App Downloads, Updates, Revenue, Purchases',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(139,'Social Media','LinkedIn Ads','https://www.linkedin.com/uas/login',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=t66e8y00mqvl&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&state={{DASHBOARD_DWID}}&scope=rw_organization_admin%20r_basicprofile%20r_member_social%20r_ads%20r_ads_reporting%20r_organization_social%20rw_ads','https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&state={{DASHBOARD_DWID}}&client_id=t66e8y00mqvl&client_secret=bmjNDKSXSq7mdOpe',NULL,'https://api.linkedin.com/v2/me?oauth2_access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(140,'Advertising','Google AdWords','https://adwords.google.com',900,220,0,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadwords+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={{ACCESS_TOKEN}}','google_adwords',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(141,'Advertising','Google AdWords (MCC)','https://adwords.google.com',900,220,0,0,1,NULL,100,1000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadwords+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={{ACCESS_TOKEN}}','google_adwords_mcc',NULL,'google_adwords_mcc_campaigns',NULL,NULL,NULL,NULL,NULL)
,(142,'Sales & Finance','HubSpot','https://www.hubspot.com',3600,200,1,0,1,NULL,100,1000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Leads, Contacts, Opportunities, Customers','','https://app.hubspot.com/oauth/authorize?client_id=164b6f5e-d0f7-11e5-a2bf-bd028721085c&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&scope=contacts&optional_scope=content%20reports%20automation%20timeline%20forms%20files','https://api.hubapi.com/oauth/v1/token?grant_type=authorization_code&client_id=164b6f5e-d0f7-11e5-a2bf-bd028721085c&client_secret=f0d882b2-4816-4b93-84c9-aff0b3a3ef48&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback%3Fstate%3D{{DASHBOARD_DWID}}&code={{OAUTH_AUTH_TOKEN}}',NULL,'https://api.hubapi.com/oauth/v1/access-tokens/{{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(143,'Support','Freshdesk','https://freshdesk.com/login',3600,200,1,0,1,NULL,100,1000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Tickets, Agents, Groups',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(144,'SEO','Google My Business','https://www.google.com/mybusiness',3600,200,1,0,1,NULL,100,2000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Views, Actions, Search',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.business.manage+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(145,'Advertising','Google AdWords','https://adwords.google.com',900,220,1,0,1,NULL,100,1000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Cost, Impressions, Clicks, Conversions',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadwords+email&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(146,'Social Media','Instagram','https://instagram.com/accounts/login/',86400,200,0,0,1,NULL,1,200,86400,'','','',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Photos, Followers, Following',NULL,'https://api.instagram.com/oauth/authorize/?client_id=bc39b1295ae44ccd91c9134455c0b94f&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&state={{DASHBOARD_DWID}}&scope=basic','https://api.instagram.com/oauth/access_token?grant_type=authorization_code&client_id=bc39b1295ae44ccd91c9134455c0b94f&client_secret=bb49822c1c604f2a85e450ca85c68bbf&code={{OAUTH_AUTH_TOKEN}}&redirect_uri=https%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(148,'Social Media','Reviews',NULL,3600,200,0,1,1,NULL,1,3,604800,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Yelp, TripAdvisor, Facebook, Google, YP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(149,'Sales & Finance','Zoho CRM','https://www.zoho.com/CRM',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Leads, Contacts, Accounts, Deals, Tasks',NULL,'https://accounts.zoho.com/oauth/v2/auth?scope=ZohoCRM.modules.ALL,ZohoCRM.users.ALL,ZohoCRM.settings.ALL&client_id=1000.1MWDG4P8WR7741047AAGLJ0UIX8NHK&response_type=code&access_type=offline&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&state={{DASHBOARD_DWID}}&prompt=consent','https://accounts.zoho.com/oauth/v2/token?code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&client_id=1000.1MWDG4P8WR7741047AAGLJ0UIX8NHK&client_secret=79a930ce498f28c320480134c4cca21a02fab4ef59&grant_type=authorization_code','https://accounts.zoho.com/oauth/v2/token?refresh_token={{REFRESH_TOKEN}}&client_id=1000.1MWDG4P8WR7741047AAGLJ0UIX8NHK&client_secret=79a930ce498f28c320480134c4cca21a02fab4ef59&grant_type=refresh_token','https://www.zohoapis.com/crm/v2/users?type=CurrentUser',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(150,'Sales & Finance','Pipedrive','https://www.pipedrive.com',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Deals, Contacts, Products',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(151,'Support','Jira','https://www.atlassian.com/software/jira',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Issues',NULL,'https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=5RH5D8MQzsLRmoI2VPnEGmxrjD50It1Q&scope=read%3Ajira-user%20read%3Ajira-work%20offline_access&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&state={{DASHBOARD_DWID}}&response_type=code&prompt=consent','https://auth.atlassian.com/oauth/token?client_id=5RH5D8MQzsLRmoI2VPnEGmxrjD50It1Q&client_secret=I4HRC2dQEr0wGCrxCsyo_MA2I28MFf1CwfxgrfE7l-oW8FAFCQm-IrHayf4-ZeKl&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&grant_type=authorization_code','https://api.atlassian.com/oauth/token?grant_type=refresh_token&client_id=5RH5D8MQzsLRmoI2VPnEGmxrjD50It1Q&client_secret=I4HRC2dQEr0wGCrxCsyo_MA2I28MFf1CwfxgrfE7l-oW8FAFCQm-IrHayf4-ZeKl&refresh_token={{REFRESH_TOKEN}}','https://api.atlassian.com/oauth/token/accessible-resources',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
;
INSERT INTO dashboard_services (id,category,service,footer_link,refresh_rate,default_height,production,featured,whitelabel,for_company_id,free_limit,premium_limit,schedule_interval,data_source_field_label,data_source_type,data_source_desc,data_source_historical,data_source_field_label2,data_source_type2,data_source_desc2,data_source_historical2,data_source_field_label3,data_source_type3,data_source_desc3,data_source_historical3,data_source_field_label4,data_source_type4,data_source_desc4,data_source_historical4,data_source_field_label5,data_source_type5,data_source_desc5,data_source_historical5,data_source_field_label6,data_source_type6,data_source_desc6,data_source_historical6,data_source_field_label7,data_source_type7,data_source_desc7,data_source_historical7,data_source_field_label8,data_source_type8,data_source_desc8,data_source_historical8,data_source_field_label9,data_source_type9,data_source_desc9,data_source_historical9,data_source_field_label10,data_source_type10,data_source_desc10,data_source_historical10,data_source_field_label11,data_source_type11,data_source_desc11,data_source_historical11,data_source_field_label12,data_source_type12,data_source_desc12,data_source_historical12,description,oauth_request,oauth_authenticate,oauth_token,oauth_refresh,oauth_account,oauth_account2,oauth_account3,oauth_account4,oauth_other_info_field,oauth_custom_url_label,oauth_custom_url_desc,db_table_name,class_name) VALUES 
(152,'Other','Trello','https://www.trello.com',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Boards, Actions','https://trello.com/1/OAuthGetRequestToken?expiration=never&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_version=1.0&response_type=token&oauth_consumer_key=80431b59afbf6336478e5c69cbdb3114&oauth_secret_key=1c8ccd1eed54d6fea905bea2f92b3b1267f4c7eb93f9d67073312fb883a157fe&return_url={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate={{DASHBOARD_DWID}}','https://trello.com/1/OAuthAuthorizeToken?name=Dashboard&scope=read&oauth_token={{OAUTH_AUTH_TOKEN}}&return_url={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth1callback%3Fstate={{DASHBOARD_DWID}}','https://trello.com/1/OAuthGetAccessToken?oauth_consumer_key=80431b59afbf6336478e5c69cbdb3114&oauth_secret_key=1c8ccd1eed54d6fea905bea2f92b3b1267f4c7eb93f9d67073312fb883a157fe&oauth_nonce={{NONCE}}&oauth_signature={{OAUTH_SIGNATURE}}&oauth_signature_method=HMAC-SHA1&oauth_timestamp={{TIME}}&oauth_token={{OAUTH_AUTH_TOKEN}}&oauth_verifier={{OAUTH_VERIFIER}}&oauth_version=1.0',NULL,'https://api.trello.com/1/tokens/{{OAUTH_AUTH_TOKEN}}/member?key=80431b59afbf6336478e5c69cbdb3114&token={{OAUTH_AUTH_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(153,'Other','Asana','https://www.asana.com',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Projects, Tasks',NULL,'https://app.asana.com/-/oauth_authorize?client_id=865726173645324&response_type=code&state={{DASHBOARD_DWID}}&scope=default&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://app.asana.com/-/oauth_token?grant_type=authorization_code&client_id=865726173645324&client_secret=97611c502d630aaf5186c4b98e4a226c&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&code={{OAUTH_AUTH_TOKEN}}','https://app.asana.com/-/oauth_token?grant_type=refresh_token&client_id=865726173645324&client_secret=97611c502d630aaf5186c4b98e4a226c&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&refresh_token={{REFRESH_TOKEN}}','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(154,'Advertising','CallRail','https://www.callrail.com',3600,200,1,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Calls, Voicemails, Texts',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(155,'Email','Email',NULL,3600,200,0,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Inbox, Unread Messages',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(156,'Reviews','Grade.Us Reviews','https://www.grade.us/home',86400,200,1,1,1,NULL,3,100,86400,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Yelp, TripAdvisor, Facebook, Google, YP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(157,'Sales & Finance','**Paypal REST API**','https://www.paypal.com',3600,200,0,0,1,NULL,100,200,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'',NULL,'https://api.paypal.com/v1/oauth2/token',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
,(158,'Web Analytics','Google Analytics 2.0','https://www.google.com/analytics/',3600,200,0,0,1,NULL,100,10000,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'',NULL,'https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.google.com%2Fanalytics%2Ffeeds+email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback&response_type=code&client_id=435984484716.apps.googleusercontent.com&access_type=offline&approval_prompt=force','https://accounts.google.com/o/oauth2/token?code={{OAUTH_AUTH_TOKEN}}&client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&redirect_uri={{PAGE_HTTPS}}://{{PAGE_HOST}}/oauth2callback&grant_type=authorization_code','https://accounts.google.com/o/oauth2/token?client_id=435984484716.apps.googleusercontent.com&client_secret=90cJDdP2e_1_H1kq7MkOVjnk&grant_type=refresh_token&refresh_token={{REFRESH_TOKEN}}','https://openidconnect.googleapis.com/v1/userinfo?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,'google_analytics_widgets','GoogleAnalyticsWidget')
,(159,'Social Media','Instagram Graph API','https://instagram.com/accounts/login/',86400,200,0,0,1,NULL,1,200,86400,'','','',0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'','',NULL,0,'Photos, Followers, Following',NULL,'https://www.facebook.com/v7.0/dialog/oauth?client_id=225327114206053&scope=read_insights,instagram_manage_insights,instagram_basic,pages_read_engagement,pages_show_list&state={{DASHBOARD_DWID}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback','https://graph.facebook.com/v7.0/oauth/access_token?client_id=225327114206053&client_secret=e53df52a2b40dc3c9e411daecb58bf5d&code={{OAUTH_AUTH_TOKEN}}&redirect_uri={{PAGE_HTTPS}}%3A%2F%2F{{PAGE_HOST}}%2Foauth2callback',NULL,'https://graph.facebook.com/v7.0/me?access_token={{ACCESS_TOKEN}}',NULL,NULL,NULL,NULL,NULL,NULL,'instagram_widgets','InstagramWidget')
;
