<?php
# Fill our vars and run on cli
# $ php -f test_db.php

$dbname = 'cyfe_db_test';
$dbuser = 'root';
$dbpass = 'root';
$dbhost = 'mysql';

$link = mysqli_connect($dbhost, $dbuser, $dbpass) or die("Unable to Connect to '$dbhost'\n");
mysqli_select_db($link, $dbname) or die("Could not open the db '$dbname'\n");

$test_query = "SHOW TABLES FROM $dbname";
$result = mysqli_query($link, $test_query);

$tblCnt = 0;
while ($tbl = mysqli_fetch_array($result)) {
    $tblCnt++;
    #echo $tbl[0]."\n";
}

if (!$tblCnt) {
    echo "There are no tables\n";
} else {
    echo "There are $tblCnt tables\n";
}
